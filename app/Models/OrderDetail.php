<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'variation',
        'price',
        'shipping_cost',
        'quantity',
        'payment_status',
        'delivery_status',
        'shipping_type',
        'pick_up_point',
        'product_referral_code'
    ];

    protected $with = [
        'product',
        'hasVariant'
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function hasVariant() {
        return $this->belongsTo(ProductVariant::class, 'variation', 'id');
    }
}
