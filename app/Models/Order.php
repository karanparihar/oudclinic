<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'shipping_address',
        'delivery_status',
        'payment_type',
        'payment_status',
        'payment_details',
        'grand_total',
        'coupan_discount',
        'code'
    ];

    protected $with = [
        'orderDetail',
        'customer'
    ];

    public function orderDetail() {
        return $this->hasMany(orderDetail::class, 'order_id', 'id');
    }

    public function customer() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


}
