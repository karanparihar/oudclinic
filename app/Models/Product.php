<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name',
       'slug',
       'category',
       'product_size',
       'tags',
       'thumbnail_img',
       'unit_price',
       'purchase_price',
       'discount',
       'quantity',
       'product_description',
       'meta_title',
       'meta_description',
       'meta_image'
    ];

    protected $with = [
        'hasCategory',
        'variant',
        'hasImages'
    ];


    public function hasCategory()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }

    public function variant()
    {
        return $this->hasMany(ProductVariant::class, 'product_id', 'id');
    }

    public function hasImages()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }
}
