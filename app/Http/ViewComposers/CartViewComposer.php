<?php
namespace App\Http\ViewComposers;

use App\Models\Cart;
use Auth;

class CartViewComposer
{
    public function compose($view)
    {
        $view->with('cart', Cart::where(['user_id' => Auth::user()->id])->get());
    }
}