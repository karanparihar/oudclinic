<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Cart;
use Response;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application home.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $slider       = Slider::whereStatus(1)->get();
        $testimonials = Testimonial::whereStatus(1)->get();
        $categories   = Category::whereStatus(1)->get();
        $products     = Product::whereStatus(1)->get();

        return view('home', compact('slider', 'testimonials', 'categories', 'products'));
    }

    /**
     * Show the application category page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function category(Request $request, $slug)
    {   
        $category  =  Category::whereSlug($slug)->first();
        if(! $category) {
            abort('404');
        }
        $products  =  Product::where(
                            ['status' => 1, 'category' => $category->id ]
                        )->get();
        $categories   = Category::whereStatus(1)->get();

        return view('product.category._index', compact('categories', 'products', 'category'));
    }


    
    /**
     * Show the application product detailed page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function productDetail($slug) {
        
        $product = Product::where('slug', $slug)->first();
        if( !empty($product) ) {
            $relatedProducts  = Product::where(['category' => $product->category])->get()->except($product->id);
             return view('product.product-detail', compact('product', 'relatedProducts'));
        }   
        abort('404');
    }

     /**
     * Change price per variation
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function variationPrice(Request $request) {

        if($request->ajax()) {
            $variant = ProductVariant::where(
                ['id' => $request->variant_id, 'product_id' => $request->product_id]
            )->first();
            if(! $variant)
                return response(['status' => trans('messages.failed')]);
            return response()->json(['state' => '200', 'data' => $variant ]);
        }
    }

    /**
     * Add to cart functionality
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function addToCart(Request $request) {
        
        if($request->ajax()) {
            $product = Product::where(['id' => $request->product_id])->first();
            if($request->variant_id) {
                $variant = ProductVariant::where(['id' => $request->variant_id])->first();
            }
            $price = $variant->price;
            $itemExist = Cart::where(['product_id' => $request->product_id, 'product_variant' => $request->variant_id, 'user_id' => Auth::user()->id])->first();
            if($itemExist) {
                $itemExist->update(['quantity' => $request->quantity]);
                $message   = trans('messages.cart.update');
            } else {
                $cartArray = array(
                    'user_id'         => Auth::user()->id ?? '',
                    'product_id'      => $product->id,
                    'product_name'    => $product->name,
                    'product_variant' => $variant->id ?? NULL,
                    'quantity'        => $request->quantity,
                    'price'           => $price,
                );
                $cartSaved =  Cart::create($cartArray);
                $message   = trans('messages.cart.add');
            }
            $cart = Cart::where(['user_id' => Auth::user()->id])->get();
            if(!empty($cart)) {
                $totalPrice = 0;
                foreach($cart as $key => $value) {
                   $totalPrice = $totalPrice + ($value->price * $value->quantity);
                }
            }
            $return[] = view('cart._index')->with(['cart' => $cart, 'totalPrice' => round($totalPrice, 2)  ])->render();
            
            return response()->json([
                'title'   => 'success', 
                'message' => $message,
                'state'   => 'success', 
                'data'    => $return, 
                'count'   => count($cart) 
            ]);
        }
    }

    /**
     * Remove item from cart.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function removeCartItem(Request $request) {

        if($request->ajax()) {
            $removed = Cart::find($request->cart_id)->delete();
            if($removed) {
                $cartItems = Cart::where(
                    [
                        'user_id' => Auth::user()->id, 
                    ]
                )->get();

                if(!empty($cartItems)) {
                    $totalPrice = 0;
                    foreach($cartItems as $key => $value) {
                       $totalPrice = $totalPrice + $value->price;
                    }
                }
                $return[] = view('cart._index')->with(['cart' => $cartItems, 'totalPrice' => round($totalPrice, 2)  ])->render();
                $message = trans('Product removed from the cart.');

                return response()->json(['title' => 'success', 'message' => $message , 'state' => 'success', 'data' => $return, 'count' => count($cartItems) ]);
            } 
        }
    }

}
