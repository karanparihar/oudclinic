<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Http\Traits\FileTrait;
use Illuminate\Support\Str;
use App\Models\Category;

class CategoryController extends Controller
{    
    use FileTrait;

    private $model_name;

    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $categories  = Category::all();
        return view('admin.pages.category._index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories  = Category::all()->pluck('id','name');
        return view('admin.category._create', compact('categories'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if($request->hasFile('banner')) {

            $data = $this->uploadImage($request->file('banner'));
            if(!empty($data)) {
                $category = Category::create(
                    array_merge(
                        $request->all(),
                        ['banner' => $data['file_path'] , 'slug' => Str::slug($request->name)]
                    )
                );
                if( ! $category)
                    return back()->with('error', trans('messages.failed'));
                return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
            }
            return back()->with('error', trans('messages.failed'));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {   
        $categories  = Category::all()->pluck('id','name');
        return view('admin.category._edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
