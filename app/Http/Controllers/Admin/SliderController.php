<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\FileTrait;
use App\Http\Requests\Slider\StoreRequest;
use App\Http\Requests\Slider\UpdateRequest;
use App\Models\Slider;

class SliderController extends Controller
{   
    use FileTrait;

    private $model_name;

    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.slider');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
       $sliders = Slider::all();
       return view('admin.slider._index', compact('sliders') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider._create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if($request->hasFile('slide')) {
            $data = $this->uploadImage($request->file('slide'));
            if(!empty($data)) {
                $slider = Slider::create(
                    array_merge(
                        $request->all(),
                        ['slide' => $data['file_path'] ]
                    )
                );
                if( ! $slider)
                    return back()->with('error', trans('messages.failed'));
                return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
            }
            return back()->with('error', trans('messages.failed'));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider._edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Slider $slider)
    {
        if($request->hasFile('slide')) {
            $data = $this->uploadImage($request->file('slide'));
            if(!empty($data)) {
                if( !$slider->update( array_merge(
                        $request->all(),
                        ['slide' => $data['file_path'] ]
                    ) ) )
                    return back()->with('error', trans('messages.failed'));
                return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));
            }
            return back()->with('error', trans('messages.failed'));

        } else {
            if( !$slider->update( $request->all()) )
                    return back()->with('error', trans('messages.failed'));
            return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()) {
        
            $slider = Slider::find($id);
            unlink(public_path('storage/'.$slider->slide));
            $delete = $slider->delete();

            if( ! $delete)
                return response(['status' => trans('messages.failed')]);

            return response(['status' => trans('messages.deleted', ['model' => $this->model_name])]);
        }
    }
}
