<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductImage;
use App\Models\ProductVariant;
use App\Models\Order;
use App\Models\OrderDetail;
use Response;
use PDF;

class CommonAjaxHandleController extends Controller
{
    
    public function deleteProductImage(Request $request) {
        
        if($request->ajax()){
            $image = ProductImage::find($request->id);
        if(! $image->delete()) 
            return response(['status' => trans('messages.failed')]);
         unlink(public_path('storage/'.$image->image));
        return response(['status' => trans('messages.deleted', ['model' => 'Product image'])]);
        }   
    }

    public function deleteProductVariant(Request $request) {
        
        if($request->ajax()){
            $variant = ProductVariant::find($request->id);
        if(! $variant->delete()) 
            return response(['status' => trans('messages.failed')]);
        return response(['status' => trans('messages.deleted', ['model' => 'Product variant'])]);
        }   
    }

    public function orderPaymentStatus(Request $request) {

        if($request->ajax()){
            $order = Order::where(['id' => $request->id])->first();
            $updated = $order->update(['payment_status' => $request->status]);
            if(! $updated) 
                return response(['status' => trans('messages.failed')]);
            return response(['status' => trans('messages.status', ['model' => 'Order payment '])]);
        } 
    }

    public function orderdeliveryStatus(Request $request) {

        if($request->ajax()){
            $order = Order::where(['id' => $request->id])->first();
            $updated = $order->update(['delivery_status' => $request->status]);
            if(! $updated) 
                return response(['status' => trans('messages.failed')]);
            return response(['status' => trans('messages.status', ['model' => 'Order delivery '])]);
        } 
    }

    public function orderGenerateInvoice(Request $request, $id) {

         $data['title'] = 'Notes List';
         $data['order'] = Order::find($id);
         $data['shipping_address'] = json_decode($data['order']->shipping_address);
         $pdf = PDF::loadView('admin.orders._invoice', $data);
         return $pdf->download('tuts_notes.pdf');
    }
}
