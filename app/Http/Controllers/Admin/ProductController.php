<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Http\Traits\FileTrait;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\ProductImage;
use Auth;

class ProductController extends Controller
{    
    use FileTrait;

    private $model_name;

    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $products = Product::orderBy('id', 'DESC')->get();

        return view('admin.products._index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Category::get()->pluck('name', 'id');

        return view('admin.products._create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {    
        if($request->hasFile('thumbnail_img')) {
            $thumbnail = $this->uploadImage( $request->file('thumbnail_img') );
        }
        if($request->hasFile('meta_image')) {
            $meta_image = $this->uploadImage( $request->file('meta_image') );
        }
        $product = Product::create( array_merge(
                $request->all(), 
                [   
                    'slug'           => Str::slug($request->name),
                    'added_by'       => Auth::user()->id,
                    'thumbnail_img'  => $thumbnail['file_path'], 
                    'meta_image'     => $meta_image['file_path'] ?? NULL
                ] 
            )
        );
        /** Product Image **/
        if($request->hasFile('gallery_images')) {
            $gallery = $this->uploadMultipleImage( $request->file('gallery_images') );
            $imagesArr = array();
            foreach($gallery as $key => $value) {
                $imagesArr[] = array(
                    'product_id' => $product->id,
                    'image'      => $value['file_path'],
                    'image_ext'  => $value['file_extension']
                );
            }
            $ProductImage = ProductImage::insert($imagesArr);
        }
        /** Product Variants **/
        if(!empty($request->size)) {
                $count = count($request->size);
                $variants = array();
                for($i = 0; $i <= $count - 1; $i++) {
                    if($request->size[$i] != null){
                          $variants[] = array(
                             'product_id' => $product->id,
                             'size'       => $request->size[$i],
                             'unit'       => $request->unit[$i],
                             'price'      => $request->price[$i],
                        );
                    }
                }
            if(!empty($variants)) {
                $productVariant = ProductVariant::insert($variants);
            }
        } 
        if( ! $product)
              return back()->with('error', trans('messages.failed'));
         return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {   
        $categories = Category::get()->pluck('name', 'id');
        return view('admin.products._edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Product $product)
    {
        if($request->hasFile('thumbnail_img')) {
            $thumbnail = $this->uploadImage( $request->file('thumbnail_img') );
        }
        if($request->hasFile('meta_image')) {
            $meta_image = $this->uploadImage( $request->file('meta_image') );
        }
        $arr = array_merge(
            $request->all(), 
            [   
                'slug'           => Str::slug($request->name),
                'added_by'       => Auth::user()->id,
                'thumbnail_img'  => $thumbnail['file_path'] ?? $product->thumbnail_img, 
                'meta_image'     => $meta_image['file_path'] ?? $product->meta_image
            ] 
        );

        /** Product Image **/
        if($request->hasFile('gallery_images')) {
            $gallery = $this->uploadMultipleImage( $request->file('gallery_images') );
            $imagesArr = array();
            foreach($gallery as $key => $value) {
                $imagesArr[] = array(
                    'product_id' => $product->id,
                    'image'      => $value['file_path'],
                    'image_ext'  => $value['file_extension']
                );
            }
            $ProductImage = ProductImage::insert($imagesArr);
        }

        /** Product Variant **/
        if(!empty($request->size)) {
            $count   = count($request->size);
            $counter = ($request->size[$count-1] == null) ? ($count - 2) : ($count - 1); 
            for($i = 0; $i <= $counter; $i++) {
                if($request->size[$i] != null) {
                    $variants= array(
                         'product_id' => $product->id,
                         'size'       => $request->size[$i],
                         'unit'       => $request->unit[$i],
                         'price'      => $request->price[$i],
                    );
                }
                $productVariant = ProductVariant::updateOrCreate(['id' => $request->variant_id[$i] ?? '' ], $variants);
            }

        }
        if(! $product->update($arr) )
              return back()->with('error', trans('messages.failed')); 
         return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()) {

            $product = Product::find($id);
            if( !empty($product->thumbnail_img) ) {
                unlink(public_path('storage/'.$product->thumbnail_img));
            }
            if( !empty($product->meta_image) ) {
                unlink(public_path('storage/'.$product->meta_image));
            }

            $delete = $product->delete();

            if( ! $delete)
                return response(['status' => trans('messages.failed')]);
            return response(['status' => trans('messages.deleted', ['model' => $this->model_name])]);
        }
    }
}
