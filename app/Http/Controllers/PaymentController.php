<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Order;
use App\Models\User;
use App\Models\OrderDetail;
use Response;
use Stripe;
use Config;
use Auth;

class PaymentController extends Controller
{   
    private $model_name;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.order');
    }

    public function payment(Request $request) {
        
        Stripe\Stripe::setApiKey(Config::get('constants.stripe.secret'));
        $payment = Stripe\Charge::create ([
                "amount"      => $request->payment * 100,
                "currency"    => "eur",
                "source"      => $request->stripeToken,
                "description" => "Test payment from Oudclinic."
        ]);

        if($payment['status'] == 'succeeded') {
        
        $cart = Cart::where(['user_id' => Auth::user()->id])->get();
            if(!empty($cart)) {
                $order = array();
                $address = array(
                    'name'    => $request->first_name.' '.$request->last_name,
                    'email'   => $request->email,
                    'address' => $request->address,
                    'city'    => $request->city,
                    'state'   => $request->state,
                    'country' => $request->country,
                    'zipcode' => $request->zipcode,
                    'phone'   => $request->phone
                );
                $shippingAddress = json_encode($address);
                $orderArr = array(
                     'user_id'          => Auth::user()->id,
                     'shipping_address' => $shippingAddress,
                     'delivery_status'  => 'Pending',
                     'payment_type'     => $request->payment_mode,
                     'payment_status'   => 'Paid',
                     'payment_details'  => json_encode($payment),
                     'grand_total'      => $request->payment,
                     'code'             => 'OUD-'.rand(0, 99999999),
                ); 

                $order = Order::create($orderArr);
                if($order) {
                    $OrderDetail = [];
                    foreach($cart as $key => $value) {
                        $OrderDetail[] = array(
                            'order_id'        => $order->id,
                            'product_id'      => $value->product_id,
                            'variation'       => $value->product_variant,
                            'price'           => $value->price,
                            'sub_total'       => sprintf('%0.2f', ($value->price * $value->quantity)),
                            'shipping_cost'   => $value->shipping_cost,
                            'quantity'        => $value->quantity,
                            'payment_status'  => 'Paid',
                            'delivery_status' => 'Pending',
                        );
                    }
                    $OrderDetail = OrderDetail::insert($OrderDetail);
                    if($OrderDetail) {
                        Cart::where(['user_id' => Auth::user()->id])->delete();
                        $userArray = array(
                           'address'          => $request->address,
                           'city'             => $request->city,
                           'state'            => $request->state,
                           'country'          => $request->country,
                           'zipcode'          => $request->zipcode,
                           'phone'            => $request->address,
                           'shipping_address' => $shippingAddress,
                           'billing_address'  => $request->save_address,
                        );
                      $user = User::where(['id' => Auth::user()->id])->update($userArray);
                          return redirect( route('order.confirmation'))->With(['success', trans('message.order.success')]);
                    } else {
                          return redirect()->back()->with(['error' => trans('message.order.error')]);
                    }
                }
            }
        }
    }


    public function orderConfirmation(Request $request) {

        return view('cart._order-confirmed');
    }
}
