<?php

namespace App\Http\Traits;

trait FileTrait {

    /**
    * upload multiple image to the database
    *
    * @return array of image paths.
    *
    */

    public function uploadImage($image) {
          $filenameWithExt = $image->getClientOriginalName ();
          $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension       = $image->getClientOriginalExtension();
          $thumbnail       = md5($filename. '_'. time()).'.'.$extension;
          $image->storeAs('public/banners/', $thumbnail);
          $dataArray = array(
              'file_path'      => 'banners/'.$thumbnail,
              'file_extension' => $extension
          );
          return $dataArray;
      
    }

    /**
    * upload multiple image to the database
    *
    * @return array of image paths.
    *
    */
    public function uploadMultipleImage($array) {
      
       $dataArray = array();
       if(!empty($array)) {

            foreach($array as $image) {
              
              $filenameWithExt = $image->getClientOriginalName ();
              $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension       = $image->getClientOriginalExtension();
              $thumbnail       = md5($filename. '_'. time()).'.'.$extension;
              $image->storeAs('public/banners/', $thumbnail);
              $dataArray[]  = array(
                  'file_path'      => 'banners/'.$thumbnail,
                  'file_extension' => $extension
              );

          }
          return $dataArray;
       }
       return false;
          
    }


}