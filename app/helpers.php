<?php

//highlights the selected navigation on admin panel
if (! function_exists('areActiveRoutes')) {
    function areActiveRoutes(Array $routes, $output = "active")
    {   
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

//highlights the selected navigation menu open on admin panel
if (! function_exists('areActiveRoutesMenuOpen')) {
    function areActiveRoutesMenuOpen(Array $routes, $output = "menu-open")
    {   
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

//highlights the selected navigation icon on admin panel
if (! function_exists('areActiveRoutesIcon')) {
    function areActiveRoutesIcon(Array $routes, $output = "fa-angle-right", $output1 = "fa-angle-left")
    {   
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            } else {
               return $output1;
            }
        }
    }
}

if(! function_exists('displayBanner')) {
    function displayBanner($path) {
        return asset('storage/'.$path);
    }
}


