var _token  = $('meta[name="csrf-token"]').attr("content");
var _appUrl = $('meta[name="app-url"]').attr("content");

jQuery(function($){

    jQuery('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({
        loading_image: 'img/loading.gif'
    });

    jQuery('#demo-1 .simpleLens-big-image').simpleLens({
        loading_image: 'img/loading.gif'
    });
    
});

function variation(product_id, variant_id) {
    $.ajax({
        type: "POST",
        url:  _appUrl+"/product-variation",
        data: {
            _token: _token,
            product_id: product_id,
            variant_id: variant_id
        },
        beforeSend: function () {
        },
        success: function (response) {
            if(response){
                $(".product-price").text(response.data.price);
                $(".prod-view-size a").removeClass('active');
                $("#"+response.data.id+"-variant").addClass('active');
                $("#product_variant").val(variant_id);
            }
        },
        complete: function (data) {
        },
        error: function (error) {
            console.log("response", response);
        },
    });
}

function addToCart(product_id) {
    if(product_id){
        var variant  = $("#product_variant").val();
        var quantity = $('#prodcut-qty :selected').text();
        $.ajax({
            type: "POST",
            url:  _appUrl+"/add-to-cart",
            data: {
                _token: _token,
                product_id: product_id,
                variant_id: variant,
                quantity: quantity
            },
            beforeSend: function () {
            },
            success: function (response) {
                if(response.state == 'success'){
                    $(".cart-badge").text(response.count);
                    $(".cartbox-summary").html(response.data);
                    swal(
                       response.title,
                       response.message,
                       response.state
                    )
                }
            },
            complete: function (data) {
               
            },
            error: function (error) {
                console.log("response", response);
            },
        });
    }
}

function removeCartItem(cart_id) {
    if(cart_id) {
        $.ajax({
            type: "POST",
            url:  _appUrl+"/remove-cart-item",
            data: {
                _token: _token,
                cart_id: cart_id,
            },
            beforeSend: function () {
            },
            success: function (response) {
                if(response.state == 'success'){
                    $(".cart-badge").text(response.count);
                    $(".cartbox-summary").html(response.data);
                    swal(
                       response.title,
                       response.message,
                       response.state
                    )
                }
            },
            complete: function (data) {
               
            },
            error: function (error) {
                console.log("response", response);
            },
        });
    }
}





