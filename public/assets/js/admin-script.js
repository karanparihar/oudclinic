var _token = $('meta[name="csrf-token"]').attr("content");
var _appUrl = $('meta[name="app-url"]').attr("content");

$(function() {
  $('.textarea').summernote()
})

$(function() {
  $("#example1").DataTable({
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: false,
    info: true,
    responsive: true,
    autoWidth: false,

  });
});

function commonDeleteAjax(url) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-danger",
      cancelButton: "btn btn-success",
    },
    buttonsStyling: true,
  });
  swalWithBootstrapButtons
    .fire({
      title: "Are you sure?",
      text: "Record will be deleted permanently.",
      icon: "error",
      showCancelButton: true,
      confirmButtonText: "Delete",
      cancelButtonText: "Cancel",
      reverseButtons: false,
    })
    .then((result) => {
      if (result.value) {
        $.ajax({
          type: "DELETE",
          url: url,
          data: {
            _token: $('meta[name="csrf-token"]').attr("content")
          },
          success: function(response) {
            if (response.status) {
              Swal.fire({
                icon: "success",
                title: "Deleted!",
                text: response.status,
              }).then((result) => {
                if (result.value) {
                  window.location.reload();
                }
              });
            } else {
              Swal.fire({
                icon: "error",
                title: "Error",
                text: response.status,
              }).then((result) => {
                if (result.value) {
                  window.location.reload();
                }
              });
            }
          },
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire("Cancelled", "Your record is safe.", "info");
      }
    });
}
function deleteMapItem(url, id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-danger",
      cancelButton: "btn btn-success",
    },
    buttonsStyling: true,
  });
  swalWithBootstrapButtons
    .fire({
      title: "Are you sure?",
      text: "Record will be deleted permanently.",
      icon: "error",
      showCancelButton: true,
      confirmButtonText: "Delete",
      cancelButtonText: "Cancel",
      reverseButtons: false,
    })
    .then((result) => {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: url,
          data: {
            _token: _token,
            id : id
          },
          success: function(response) {
            if (response.status) {
              Swal.fire({
                icon: "success",
                title: "Deleted!",
                text: response.status,
              }).then((result) => {
                if (result.value) {
                  window.location.reload();
                }
              });
            } else {
              Swal.fire({
                icon: "error",
                title: "Error",
                text: response.status,
              }).then((result) => {
                if (result.value) {
                  window.location.reload();
                }
              });
            }
          },
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire("Cancelled", "Your record is safe.", "info");
      }
    });
}

$("#add_more").click(function() {
    var rowData = $('#myTable1 tbody tr:last td:eq(2) input').val();
        if(rowData =='' || rowData ==null || rowData =='undefined') {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-center',
              showConfirmButton: true,
              timer: 10000
            });  
            Toast.fire({
                icon: 'warning',
                title: 'Please fill the previous row first then proceed to next.'
            });
            return false;
    }
    var increment = 1;
    var html = $("#row").children("#myTable tbody tr:last").html();
    $("#myTable tbody").append("<tr>" + html + "</tr>");
    $("#myTable tbody tr:gt(0) td:last").html("");
    $("#myTable tbody tr:gt(0) td:last").append('<button type="button" class="remove btn btn-danger"><i class="fa fa-minus" title="Remove"></i></button>');
});

$("#myTable").on("click", '.remove', function() {
    $(this).closest("tr").remove();
});

function changeOrderPaymentStatus(val){
   var status = val.value;
   var orderId       = $("#order_id").val();
   if(status) {
      const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-danger",
        cancelButton: "btn btn-success",
      },
      buttonsStyling: true,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "To change the order status",
        icon: "error",
        showCancelButton: true,
        confirmButtonText: "Change",
        cancelButtonText: "Cancel",
        reverseButtons: false,
      })
      .then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: _appUrl +'/admin/order-payment-status',
            data: {
              _token: _token,
              status : status,
              id     : orderId
            },
            success: function(response) {
              if (response.status) {
                Swal.fire({
                  icon: "success",
                  title: "success!",
                  text: response.status,
                }).then((result) => {
                  if (result.value) {
                    window.location.reload();
                  }
                });
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Error",
                  text: response.status,
                }).then((result) => {
                  if (result.value) {
                    window.location.reload();
                  }
                });
              }
            },
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire("Cancelled", "Your record is safe.", "info");
        }
      });
   }
}

function changeOrderDeliveryStatus(val){
   var status = val.value;
   var orderId       = $("#order_id").val();
   if(status) {
      const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-danger",
        cancelButton: "btn btn-success",
      },
      buttonsStyling: true,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "To change the order status",
        icon: "error",
        showCancelButton: true,
        confirmButtonText: "Change",
        cancelButtonText: "Cancel",
        reverseButtons: false,
      })
      .then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: _appUrl +'/admin/order-delivery-status',
            data: {
              _token: _token,
              status : status,
              id     : orderId
            },
            success: function(response) {
              if (response.status) {
                Swal.fire({
                  icon: "success",
                  title: "success!",
                  text: response.status,
                }).then((result) => {
                  if (result.value) {
                    window.location.reload();
                  }
                });
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Error",
                  text: response.status,
                }).then((result) => {
                  if (result.value) {
                    window.location.reload();
                  }
                });
              }
            },
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire("Cancelled", "Your record is safe.", "info");
        }
      });
   }
}


