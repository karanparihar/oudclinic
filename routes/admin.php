<?php

/*
|--------------------------------------------------------------------------
| Web Routes ::  Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth', 'isAdmin'], 'prefix'=> 'admin'], function() {
      
     Route::get('dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');
     Route::resource('change-password', App\Http\Controllers\Admin\ChangePasswordController::class, ['except' => ['show', 'edit', 'destroy','update', 'create']]);
     Route::resource('users', App\Http\Controllers\Admin\UserController::class, ['except' => []]);
     Route::resource('customers', App\Http\Controllers\Admin\CustomerController::class, ['except' => []]);
     Route::resource('products', App\Http\Controllers\Admin\ProductController::class, ['except' => []]);
     Route::resource('categories', App\Http\Controllers\Admin\CategoryController::class, ['except' => []]);
     Route::resource('contacts', App\Http\Controllers\Admin\ContactRequestController::class, ['except' => []]);
     Route::resource('slider', App\Http\Controllers\Admin\SliderController::class, ['except' => []]);
     Route::resource('orders', App\Http\Controllers\Admin\OrderController::class, ['except' => []]);
     Route::resource('testimonial', App\Http\Controllers\Admin\TestimonialController::class, ['except' => []]);


     /* *Ajax Web Routes **/
     Route::post('/product-delete-image', [App\Http\Controllers\Admin\CommonAjaxHandleController::class, 'deleteProductImage'])->name('product.image.delete');
     Route::post('/product-delete-variant', [App\Http\Controllers\Admin\CommonAjaxHandleController::class, 'deleteProductVariant'])->name('product.variant.delete');
     Route::post('/order-payment-status', [App\Http\Controllers\Admin\CommonAjaxHandleController::class, 'orderPaymentStatus'])->name('order.payment.status');
     Route::post('/order-delivery-status', [App\Http\Controllers\Admin\CommonAjaxHandleController::class, 'orderdeliveryStatus'])->name('order.delivery.status');
    Route::get('/order-invoice/{id}', [App\Http\Controllers\Admin\CommonAjaxHandleController::class, 'orderGenerateInvoice'])->name('order.invoice');
     

     
});



