<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Auth::routes(['verify' => true]); 

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/category/{slug}', [App\Http\Controllers\HomeController::class, 'category'])->name('category.all');
Route::get('/product/{slug}', [App\Http\Controllers\HomeController::class, 'productDetail'])->name('product.detail');
Route::post('/product-variation', [App\Http\Controllers\HomeController::class, 'variationPrice'])->name('product.variation');
Route::post('/add-to-cart', [App\Http\Controllers\HomeController::class, 'addToCart'])->name('product.cart');
Route::post('/remove-cart-item', [App\Http\Controllers\HomeController::class, 'removeCartItem'])->name('product.cart.remove');

Route::resource('contact-us', App\Http\Controllers\ContactusController::class, ['except' => ['create', 'edit', 'update', 'show', 'destroy']]);
Route::get('/about-us', [App\Http\Controllers\AboutusController::class, 'index'])->name('about-us');
Route::get('/services', [App\Http\Controllers\ServicesController::class, 'index'])->name('services');
Route::resource('checkout', App\Http\Controllers\CheckoutController::class, ['except' => ['create', 'edit', 'update', 'show', 'destroy']]);

Route::post('/payment', [App\Http\Controllers\PaymentController::class, 'payment'])->name('payment.make');
Route::post('/payment-form', [App\Http\Controllers\PaymentController::class, 'paymentForm'])->name('payment.form');
Route::get('/order-confirmation', [App\Http\Controllers\PaymentController::class, 'orderConfirmation'])->name('order.confirmation');

include('admin.php');

include('customer.php');