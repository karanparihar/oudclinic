<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->integer('category')->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->string('product_size')->nullable();
            $table->string('tags');
            $table->text('gallery_images');
            $table->text('thumbnail_img');
            $table->string('unit_price');
            $table->string('purchase_price');
            $table->string('discount');
            $table->string('quantity');
            $table->text('product_description');
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
