@extends('layouts.front')
@section('title', 'Home')
@section('content')

  <!-- Start slider -->
  <section id="slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        @if(!empty($slider))
        @foreach($slider as $key => $value)
        <div class="item @if($key==0) active @endif">
          <img src="{{ displayBanner($value->slide) }}" alt="slide 1" class="w-100">
        </div>
        @endforeach
        @endif

      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="fa fa-angle-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="fa fa-angle-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>
  <!-- / slider -->
  <!-- Products section -->
  <section id="product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="product-area">
              <div class="product-inner">
                <h2 class="section-title">Best Sellers</h2>
                <div class="row">
                  @if(!empty($products))
                    @foreach($products as $key => $value)
                      <div class="col-xs-6 col-sm-3 col-lg-3 product-catg">
                        <figure>
                          <div class="relative">
                            <a class="product-img" href="{{ route('product.detail', Str::slug($value->name) ) }}">
                              <img src="{{ displayBanner($value->thumbnail_img) }}" alt="{{ $value->slug }}" style="height:250px; width:300px;">
                            </a>
                            <a class="add-card-btn" href="{{ route('product.detail', $value->slug ) }}">Buy Now</a>
                          </div>
                          <figcaption>
                            <h4 class="product-title">
                              <a href="{{ route('product.detail', $value->slug ) }}" alt="{{ $value->slug }}">{{ $value->name }}</a>
                            </h4>
                            <span class="product-price">{{ trans('app.currency') }}{{ $value->purchase_price }}</span>
                          </figcaption>
                        </figure>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>       
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Products section -->
  <!-- banner section -->
  <section id="banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">        
          <div class="row">
            <div class="banner-area">
            <a href="#"><img src="{{ asset('assets/site/img/ad-banner.jpg') }}" alt="ad-banner img"></a>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Category section -->
  <section id="category">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="product-area">
              <div class="product-inner">
                <h2 class="section-title">Shop By Category</h2>
                <div class="carousel row" data-flickity='{"cellAlign": "left", "lazyLoad": true, "wrapAround": true, "pageDots": false, "initialIndex": 2, "autoPlay": false }'>
                  @if(!empty($categories))
                    @foreach($categories as $key => $value)
                      <div class="carousel-cell"> 
                        <figure>
                          <div class="relative">
                            <a class="product-img" href="{{ route('category.all', $value->slug) }}">
                              <img src="{{ displayBanner($value->banner) }}" alt="{{ $value->slug }}" style="height:250px; width:300px;">
                          </a>
                          </div>
                          <figcaption>
                            <h4 class="product-title text-center"><a href="#">{{ $value->name }}</a></h4>
                          </figcaption>
                        </figure>    
                      </div>
                    @endforeach
                  @endif
                 
                </div>
              </div>
            </div>       
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Products section -->
  <!-- Testimonial -->
  <section id="testimonial"> 
    <div id="myCarouseltesti" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarouseltesti" data-slide-to="0" class="active"></li>
        <li data-target="#myCarouseltesti" data-slide-to="1"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        @if(!empty($testimonials))
          @foreach($testimonials as $key => $value)
            <div class="item @if($key==0) active @endif">
              <div class="testimonial-single">
                <img class="testimonial-img" src="{{ displayBanner($value->image) }}" alt="testimonial img">
                  <span class="fa fa-quote-left testimonial-quote"></span>
                  <p>{{ $value->description }}</p>
                  <div class="testimonial-info">
                    <p>{{ $value->name }}</p>
                  </div>
              </div>
            </div>
          @endforeach
        @endif

      </div>
    </div> 
  </section>
  <!-- / Testimonial -->
  <!-- / Footer Features -->
  <div class="footer-features hidden-xs">
    <div class="container">
      <ul class="features-list d-flex justify-content-between">
        <li class="feature text-center">
          <img src="{{ asset('assets/site/img/payment-security.png') }}">
          <span class="feature-text">100% Payment <br>Secured</span>
        </li>
        <li class="feature text-center">
          <img src="{{ asset('assets/site/img/wallet.png') }}">
          <span class="feature-text">Support lots <br>of Payments</span>
        </li>
        <li class="feature text-center">
          <img src="{{ asset('assets/site/img/support.png') }}">
          <span class="feature-text">24 hours/7 days <br>Support</span>
        </li>
        <li class="feature text-center">
          <img src="{{ asset('assets/site/img/truck.png') }}">
          <span class="feature-text">Free Delivery <br>with $50</span>
        </li>
        <li class="feature text-center">
          <img src="{{ asset('assets/site/img/discount.png') }}">
          <span class="feature-text">Best Price <br>Guaranteed</span>
        </li>
      </ul>
    </div>
  </div>

@endsection
