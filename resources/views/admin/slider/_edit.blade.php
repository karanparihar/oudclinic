@extends('admin.layouts._master')

@section('title', 'Edit Slider')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
        
        {!! Form::open(['route' => ['slider.update', $slider->id], 'files' => true, 'method'=>'PUT']) !!}

        <div class="card-body">
            <div class="form-group">
              {!! Form::label('slide', 'Banner', ['class' => 'required']) !!}<sup class="text-danger">[Required]</sup>
              {!! Form::file('slide', array_merge(['class' => 'form-control'], ['required' => false] ) ) !!}
              @error('slide')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            </div>
            <input type="hidden" name="banner_db" value="{{ $slider->slide }}">

            <div class="form-group">
	            {!! Form::label('short_description', 'Short Description', ['class' => 'awesome']) !!} 
	            {!! Form::textarea('short_description', old('short_description', $slider->short_description), array_merge(['class' => 'form-control'], ['required' => false], ['rows' => 4] ) ) !!}
	             @error('short_description')
	                  <span class="invalid-feedback" role="alert">
	                      <strong>{{ $message }}</strong>
	                  </span>
	              @enderror
            </div>

            <div class="form-group">
              {!! Form::label('Status', 'Status', ['class' => 'required']) !!}
              {!! Form::select('status', [
                       '1' => trans('app.dropdown.active'), 
                       '0' => trans('app.dropdown.inactive')
                      ],
                    $slider->status,
                    ['class' => 'form-control','placeholder' => 'Pick an option...', 'required' => true]) 
                !!}
                @error('status')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror  
            </div>
        </div>
        <div class="card-footer">
            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
            {!! link_to_route('slider.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
            {!! Form::close() !!}
        </div>

      </div>
      <!-- /.card -->
    </div>

    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Preview</h3>

        </div>

        <div class="card-body">
            <div class="form-group img-fluid">
                 <img src="{{ displayBanner($slider->slide) }}" height="400px" width="500px">
            </div>
        </div>
    
      </div>
      <!-- /.card -->
    </div>
   
  </div>

</section>
<!-- /.content -->

@endsection