@extends('admin.layouts._master')

@section('title', 'Add Testimonial')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
        
        {!! Form::open(['route' => 'testimonial.store', 'files' => true]) !!}
        <div class="card-body">
            
            <div class="form-group">
               {!! Form::label('name', 'Name', ['class' => 'required']) !!}
               <sup class="text-danger">[Required]</sup>
               {!! Form::text('name', old('name'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
              {!! Form::label('image', 'Image', ['class' => 'required']) !!}
              <sup class="text-danger">[Required]</sup>
              {!! Form::file('image', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('image')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            </div>

            <div class="form-group">
	            {!! Form::label('description', 'Feedback', ['class' => 'awesome']) !!} 
              <sup class="text-danger">[Required]</sup>
	            {!! Form::textarea('description', old('description'), array_merge(['class' => 'form-control'], ['required' => true], ['rows' => 4] ) ) !!}
	             @error('description')
	                  <span class="invalid-feedback" role="alert">
	                      <strong>{{ $message }}</strong>
	                  </span>
	              @enderror
          </div>
        </div>
        <div class="card-footer">
            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
            {!! link_to_route('testimonial.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
            {!! Form::close() !!}
        </div>

      </div>
      <!-- /.card -->
    </div>
   
  </div>

</section>
<!-- /.content -->

@endsection