@extends('admin.layouts._master')

@section('title', 'Testimonial')

@section('content')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card card-primary card-outline">
               <div class="card-header">
                  <a style="float: right;" href="{{ route('testimonial.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Testimonial</a>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Image</th>
                           <th>Feedback</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @if(!empty($testimonials))
                     @foreach($testimonials as $value)
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value->name }}</td>
                           <td>
                              <img src="{{ displayBanner($value->image) }}" width="80" height="80">
                           </td>
                           <td>{{ Str::limit($value->description, 80) }}</td>
                           <td>
                               <a href="{{ route('slider.edit', $value->id) }}" title="{{ trans('app.actions.edit') }}">
                                 <i class="fas fa-edit text-warning"></i>
                              </a>&nbsp;|&nbsp;
                              <a href="javascript:;" onclick="commonDeleteAjax('{{ route('slider.destroy', $value->id)}}');" title="Delete section">
                                 <i class="fas fa-trash text-danger" aria-hidden="true"></i>
                              </a> 
                           </td>
                        </tr>
                     @endforeach
                     @endif
                     </tbody>
                    
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>

@endsection