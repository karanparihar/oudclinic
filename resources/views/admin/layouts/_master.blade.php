<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{ URL::to('/') }}">
    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
    <!-- Font Awesome -->
    @include('admin.partials.inc._head')
    <style>
        .invalid-feedback { display: block !important; }
    </style>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('admin.partials._header')
        @include('admin.partials._sidebar') 
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">  
             @include('admin.partials._breadcrums')
             @yield('content')
        </div>
        @include('admin.partials._footer')
    </div>
     @include('admin.partials.inc._foot')
     @include('admin.partials._alerts')
  </body>
</html>
