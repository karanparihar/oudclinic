@extends('admin.layouts._master')

@section('title', 'Order Detail')

@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="col-12">
	             <div class="callout callout-info">
	               <div class="row">
	               	    <input type="hidden" name="order_id" value="{{ $order->id }}" id="order_id">
	               	    <div class="col-md-6">
		                	<label>Payment Status</label>
		                	<select class="form-control" name="delivery_status" onchange="changeOrderPaymentStatus(this);">
		                		<option value="Paid" {{ $order->payment_status == 'Paid' ? 'selected' : ''}}>Paid</option>
		                		<option value="Unpaid" {{ $order->payment_status == 'Unpaid' ? 'selected' : ''}}>Unpaid</option>
		                	</select>
		                </div>
		                <div class="col-md-6">
		                	<label>Delivery Status</label>
		                	<select class="form-control" name="delivery_status" onchange="changeOrderDeliveryStatus(this);">
		                		<option value="Pending" {{ $order->payment_status == 'Pending' ? 'selected' : ''}}>Pending</option>
		                		<option value="Confirmed" {{ $order->payment_status == 'Confirmed' ? 'selected' : ''}}>Confirmed</option>
		                		<option value="On delivery" {{ $order->payment_status == 'On delivery' ? 'selected' : ''}}>On Delivery</option>
		                		<option value="Delivered" {{ $order->payment_status == 'Delivered' ? 'selected' : ''}}>Delivered</option>
		                		<option value="Cancel" {{ $order->payment_status == 'Cancel' ? 'selected' : ''}}>Cancel</option>
		                	</select>
		                </div>
	                 </div>
	            </div>
            </div>
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
               
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>Admin, Inc.</strong><br>
                    795 Folsom Ave, Suite 600<br>
                    San Francisco, CA 94107<br>
                    Phone: (804) 123-5432<br>
                    Email: info@almasaeedstudio.com
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>{{ $shippingAddress->name ?? '' }}</strong><br>
                    {{ $shippingAddress->address ?? ''  }}<br>
                    {{ $shippingAddress->city ?? ''}} , {{ $shippingAddress->state ?? '' }} {{ $shippingAddress->zip_code ?? '' }}<br>
                    Phone: {{ $shippingAddress->phone ?? ''}} <br>
                    Email: {{ $shippingAddress->email ?? '' }}
                  </address>
                  
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Order# : {{ $order->code }}</b><br>
                  <b>Order Status:</b> {{ $order->delivery_status }}<br>
                  <b>Order Date:</b> {{ $order->created_at }}<br>
                  <b>Total Amount:</b> £{{ $order->grand_total }}<br>
                  <b>Payment Method:</b> {{ $order->payment_type }}
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Photo</th>
                      <th>Product</th>
                      <th>Delivery Type</th>
                      <th>QTY</th>
                      <th>Price</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($order->orderDetail) > 0)
                        @php $i = 0; @endphp
                        @foreach($order->orderDetail as $key => $value)
		                    <tr>
		                      <td>{{ ++$i }}</td>
		                      <td><img src="{{ displayBanner($value->product['thumbnail_img'])}}" width="80" height="80"></td>
		                      <td>{{ $value->product['name'] }} ({{$value->hasVariant['size']}}{{$value->hasVariant['unit']}})</td>
		                      <td>{{ $value->shipping_type }}</td>
		                      <td>{{ $value->quantity }}</td>
		                      <td>£{{ $value->price }}</td>
		                      <td>£{{ $value->sub_total }}</td>
		                    </tr>
	                    @endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                 <div class="col-6">
              
                </div>
                <!-- /.col -->
                <div class="col-6">
             
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>£{{ $order->grand_total }}</td>
                      </tr>
                      <tr>
                        <th>Tax (0.0%)</th>
                        <td>£0.00</td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td>£0.00</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>£{{ $order->grand_total }}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  
                  <a href="{{ route('order.invoice', $order->id) }}" class="btn btn-primary float-right" style="margin-right: 5px;"><i class="fas fa-download"></i> Generate Invoice
                  </a>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection