<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>A simple, clean, and responsive HTML invoice template</title>

		<style>
			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr td:nth-child(2) {
				text-align: right;
			}

			.invoice-box table tr.top table td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				padding-bottom: 40px;
			}

			.invoice-box table tr.heading td {
				background: #eee;
				border-bottom: 1px solid #ddd;
				font-weight: bold;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 1px solid #eee;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total td:nth-child(2) {
				border-top: 2px solid #eee;
				font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}

			/** RTL **/
			.invoice-box.rtl {
				direction: rtl;
				font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
			}

			.invoice-box.rtl table {
				text-align: right;
			}

			.invoice-box.rtl table tr td:nth-child(2) {
				text-align: left;
			}
		</style>
	</head>

	<body>
		<div class="invoice-box">
			<table cellpadding="0" cellspacing="0">
				<tr class="top">
					<td colspan="2">
						<table>
							<tr>
								<td class="title">
									<img src="https://www.sparksuite.com/images/logo.png" style="width: 100%; max-width: 300px" />
								</td>

								<td>
									Invoice #: 123<br />
									Created: January 1, 2015<br />
									Due: February 1, 2015
								</td>
							</tr>
						</table>
					</td>
				</tr>

			 <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
               
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>Admin, Inc.</strong><br>
                    795 Folsom Ave, Suite 600<br>
                    San Francisco, CA 94107<br>
                    Phone: (804) 123-5432<br>
                    Email: info@almasaeedstudio.com
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>{{ $shippingAddress->name ?? '' }}</strong><br>
                    {{ $shippingAddress->address ?? ''  }}<br>
                    {{ $shippingAddress->city ?? ''}} , {{ $shippingAddress->state ?? '' }} {{ $shippingAddress->zip_code ?? '' }}<br>
                    Phone: {{ $shippingAddress->phone ?? ''}} <br>
                    Email: {{ $shippingAddress->email ?? '' }}
                  </address>
                  
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Order# : {{ $order->code }}</b><br>
                  <b>Order Status:</b> {{ $order->delivery_status }}<br>
                  <b>Order Date:</b> {{ $order->created_at }}<br>
                  <b>Total Amount:</b> £{{ $order->grand_total }}<br>
                  <b>Payment Method:</b> {{ $order->payment_type }}
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Photo</th>
                      <th>Product</th>
                      <th>Delivery Type</th>
                      <th>QTY</th>
                      <th>Price</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($order->orderDetail) > 0)
                        @php $i = 0; @endphp
                        @foreach($order->orderDetail as $key => $value)
		                    <tr>
		                      <td>{{ ++$i }}</td>
		                      <td><img src="{{ displayBanner($value->product['thumbnail_img'])}}" width="80" height="80"></td>
		                      <td>{{ $value->product['name'] }}</td>
		                      <td>{{ $value->shipping_type }}</td>
		                      <td>{{ $value->quantity }}</td>
		                      <td>£{{ $value->price }}</td>
		                      <td>£{{ $value->sub_total }}</td>
		                    </tr>
	                    @endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                 <div class="col-6">
              
                </div>
                <!-- /.col -->
                <div class="col-6">
             
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>£{{ $order->grand_total }}</td>
                      </tr>
                      <tr>
                        <th>Tax (0.0%)</th>
                        <td>£0.00</td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td>£0.00</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>£{{ $order->grand_total }}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  
                  <a href="{{ route('order.invoice', $order->id) }}" class="btn btn-primary float-right" style="margin-right: 5px;"><i class="fas fa-download"></i> Generate Invoice
                  </a>
                </div>
              </div>
            </div>
			</table>
		</div>
	</body>
</html>