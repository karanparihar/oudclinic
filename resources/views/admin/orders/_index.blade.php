@extends('admin.layouts._master')

@section('title', 'Orders')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card card-primary card-outline">
               <div class="card-header">
                  
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Order Code</th>
                           <th>Customer</th>
                           <th>Num. of products</th>
                           <th>Amount</th>
                           <th>Payment Status</th>
                           <th>Delivery Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(!empty($orders))
                        @php $counter = 0; @endphp
                        @foreach($orders as $key => $value)
                        <tr>
                           <td>{{ ++$counter }}</td>
                           <td>{{ $value->code }}</td>
                           <td>{{ $value->customer['first_name'] }} {{$value->customer['last_name'] }}</td>
                           <td class="text-center">{{ count($value->orderDetail) }}</td>
                           <td>£{{ $value->grand_total }}</td>
                           <td class="text-center">
                              @if($value->payment_status == 'Paid')
                                 <span class="badge badge-success">{{ $value->payment_status}}</span>
                              @else
                                 <span class="badge badge-danger">{{ $value->payment_status}}</span>
                              @endif
                           </td>
                           <td class="text-center">
                              @if($value->delivery_status == 'Pending')
                                  <span class="badge badge-danger">{{ $value->delivery_status}}</span>
                              @elseif($value->delivery_status == 'Shipped')
                                  <span class="badge badge-success">{{ $value->delivery_status}}</span>
                              @else
                                  <span class="badge badge-warning">{{ $value->delivery_status}}</span>
                              @endif
                           </td>

                           <td>
                              <a href="{{ route('orders.show', $value->id) }}" title="{{ trans('app.actions.show') }}"><i class="fas fa-eye text-success"></i>
                              </a> | <a href="{{ route('order.invoice', $value->id) }}"><i class="fas fa-download text-warning"></i>
                              </a> | <a href="javascript:;" onclick="commonDeleteAjax('{{ route('orders.destroy', $value->id)}}');" title="{{ trans('app.actions.delete') }}"><i class="fas fa-trash text-danger"></i>
                              </a> 
                           </td>
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>

@endsection