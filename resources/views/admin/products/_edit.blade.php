@extends('admin.layouts._master') @section('title', 'Edit Product') @section('content')

<!-- Main content -->
<style type="text/css">
#image-container { overflow:auto; }
#image-container .image { width:100px;height:100px;float:left;position:relative; }
a.delete { display:none;position:absolute;top:0;right:0;width:30px;height:30px;text-indent:-999px;background:red; }
.image:hover a.delete { display:block; }
</style>
<section class="content">
    
    {!! Form::open(['route' => ['products.update', $product->id], 'files' => true, 'method'=>'PUT']) !!}
    
    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Information</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('name', 'Name', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('name', old('name', $product->name), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Product name'] ) ) !!} 
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('category', 'Category', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::select('category', $categories, $product->category, ['class' => 'form-control','placeholder' => 'Select category', 'required' => true]) !!} 
                        @error('category')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('product_size', 'Product Size', ['class' => 'required']) !!}
                        {!! Form::text('product_size', old('product_size', $product->product_size), array_merge(['class' => 'form-control'], ['required' => false, 'placeholder' => 'Product size'] ) ) !!}
                        @error('product_size')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product price + stock</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('unit_price', 'Unit Price', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('unit_price', old('unit_price', $product->unit_price), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Unit price'] ) ) !!} 
                        @error('unit_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('purchase_price', 'Purchase price', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('purchase_price', old('purchase_price', $product->purchase_price), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Purchase price'] ) ) !!} @error('purchase_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('discount', 'Discount', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('discount', old('discount', $product->discount), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Discount'] ) ) !!}
                        @error('discount')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('quantity ', 'Quantity ', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('quantity', old('quantity', $product->quantity), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Quantity'] ) ) !!} 
                        @error('quantity')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Variants</h3>
                </div>
                <div class="card-body">
                   <div class="table-responsive">
                      <table class="table table-bordered da-table" width="100%" id="myTable" cellspacing="0">
                     <thead>
                        <tr>
                           <th>Size</th>
                           <th>Unit</th>
                           <th>price</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody id="row">
                        @if(!empty($product->variant))
                            @foreach($product->variant as $key => $value)
                                <tr>
                                    <td>
                                      <input type="text" name="size[]" value="{{ $value->size }}" class="form-control">
                                   </td>
                                    <td>
                                     <input type="text" name="unit[]" value="{{ $value->unit }}" class="form-control">
                                   </td>
                                   <td>
                                      <input type="text" name="price[]" value="{{ $value->price }}" class="form-control">
                                   </td>
                                   <td>
                                   <a href="javascript:;"onclick="deleteMapItem('{{ route('product.variant.delete')}}', {{$value->id}});">
                                        <i class="fa fa-trash text-danger" title="Delete Product Variant"></i>
                                    </a>
                                   </td>
                                </tr>
                                <input type="hidden" name="variant_id[]" value="{{ $value->id }}">
                           @endforeach
                        @endif
                        <tr>
                            <td>
                              <input type="text" name="size[]" class="form-control">
                           </td>
                            <td>
                             <input type="text" name="unit[]" class="form-control" value="">
                           </td>
                           <td>
                              <input type="text" name="price[]" class="form-control">
                           </td>
                           <td>
                              <button type="button"  id="add_more" class="btn btn-success"><i class="fa fa-plus" title="Add More"></i></button>
                           </td>
                        </tr>
                     </tbody>
                      </table>
                  </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Images</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('gallery-images', 'Product Images', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::file('gallery_images[]', array_merge(['class' => 'form-control'], ['required' => false, 'multiple' => true] ) ) !!}
                        
                        @if(!empty($product->hasImages))
                           <div id="image-container">
                            @foreach($product->hasImages as $key => $value)  
                              <img style="margin-top:10px" height="100" width="100" class="img" src="{{ displayBanner($value->image) }}"/>
                                <a href="javascript:;"  onclick="deleteMapItem('{{ route('product.image.delete')}}', {{$value->id}});">
                                    <i class="fa fa-trash text-danger" title="Delete Product Image?"></i>
                                </a>     
                            @endforeach
                            </div>
                        @endif
                        
                        <input type="hidden" name="db_gallery" value="{{ $product->gallery_images }}">
                    </div>

                    <div class="form-group">
                        {!! Form::label('thumbnail_img', 'Thumbnail Image (250x300)', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::file('thumbnail_img', array_merge(['class' => 'form-control'], ['required' => false] ) ) !!}
                        <div id="images" style="white-space: nowrap;margin-top: 10px;">
                             <img src="{{ displayBanner($product->thumbnail_img) }}" alt="" style="width:80px; height: 80px;" />
                             
                        </div>
                      
                        <input type="hidden" name="db_thumb" value="{{ $product->thumbnail_img }}">
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
      
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Description</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('product_description', 'Description', ['class' => 'awesome']) !!} 
                        {!! Form::textarea('product_description', old('product_description', $product->product_description), array_merge(['class' => 'form-control textarea'],
                        ['required' => true], ['rows' => 10] ) ) !!} @error('product_description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('tags', 'Highlights', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::textarea('tags', old('tags', $product->tags), array_merge(['class' => 'form-control textarea'], ['required' => true, 'placeholder' => ''] ) ) !!} 
                    
                        @error('tags')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">SEO Meta Tags</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('meta_title ', 'Meta title ', ['class' => 'required']) !!} 
                        {!! Form::text('meta_title', old('meta_title', $product->meta_title), array_merge(['class' => 'form-control'], ['required' => false, 'placeholder' => 'Meta title'] ) ) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('meta_description', 'Meta Description', ['class' => 'awesome']) !!}
                        {!! Form::textarea('meta_description', old('meta_description', $product->meta_description), array_merge(['class' => 'form-control'], ['required' =>
                        false], ['rows' => 4] ) ) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('meta_image', 'Meta Image (300x300)', ['class' => 'required']) !!}
                        {!! Form::file('meta_image', array_merge(['class' => 'form-control'], ['required' => false] ) ) !!}
                    </div>
                </div>
                <!-- /.card-body -->
                   <div class="card-footer">
                        
                        {!! link_to_route('categories.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
                        {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success float-right']) !!}
                        {!! Form::close() !!}
                    </div>
            </div>
            <!-- /.card -->
        </div>
    </div>

    {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection
