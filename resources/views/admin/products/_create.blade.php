@extends('admin.layouts._master') @section('title', 'Add Product') @section('content')

<!-- Main content -->
<section class="content">
    
    {!! Form::open(['route' => 'products.store', 'files' => true]) !!}
    
    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Information</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('name', 'Name', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('name', old('name'), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Product name'] ) ) !!} 
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('category', 'Category', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::select('category', $categories, '', ['class' => 'form-control','placeholder' => 'Select category', 'required' => true]) !!} 
                        @error('category')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('product_size', 'Product Size', ['class' => 'required']) !!}
                        {!! Form::text('product_size', old('product_size'), array_merge(['class' => 'form-control'], ['required' => false, 'placeholder' => 'Product size'] ) ) !!}
                        @error('product_size')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
         <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product price + stock</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('unit_price', 'Unit Price', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('unit_price', old('unit_price', 0), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Unit price'] ) ) !!} 
                        @error('unit_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('purchase_price', 'Purchase price', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('purchase_price', old('purchase_price', 0), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Purchase price'] ) ) !!} @error('purchase_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('discount', 'Discount', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('discount', old('discount', 0), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Discount'] ) ) !!}
                        @error('discount')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('quantity ', 'Quantity ', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::text('quantity', old('quantity', 0), array_merge(['class' => 'form-control'], ['required' => true, 'placeholder' => 'Quantity'] ) ) !!} 
                        @error('quantity')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    
    </div>

    <div class="row">
         <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Variants</h3>
                </div>
                <div class="card-body">
                   <div class="table-responsive">
                      <table class="table table-bordered da-table" width="100%" id="myTable" cellspacing="0">
                     <thead>
                        <tr>
                           <th>Size</th>
                           <th>Unit</th>
                           <th>price</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody id="row">
                        <tr>
                            <td>
                              <input type="text" name="size[]" class="form-control" value="">
                           </td>
                            <td>
                            <input type="text" name="unit[]" class="form-control" value="" placeholder="ML, MM ">
                           </td>
                           <td>
                              <input type="text" name="price[]" class="form-control" value="">
                           </td>
                           <td>
                              <button type="button"  id="add_more" class="btn btn-success"><i class="fa fa-plus" title="Add More"></i></button>
                           </td>
                        </tr>
                     </tbody>
                      </table>
                  </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Images</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('gallery-images', 'Product Images', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::file('gallery_images[]', array_merge(['class' => 'form-control'], ['required' => true, 'multiple' => true] ) ) !!}
                        <small class="text-info">These images are visible in product details page.</small>
                        @error('gallery_images')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('thumbnail_img', 'Thumbnail Image (250 x 300)', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::file('thumbnail_img', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
                        <small class="text-info">
                            This image is visible in all product box. Use 300x300 sizes image.
                        </small>
                        @error('thumbnail_img')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Product Description</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('product_description', 'Description', ['class' => 'awesome']) !!} {!! Form::textarea('product_description', old('product_description'), array_merge(['class' => 'form-control textarea'],
                        ['required' => true], ['rows' => 10] ) ) !!} @error('product_description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('tags', 'Highlight', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
                        {!! Form::textarea('tags', old('tags'), array_merge(['class' => 'form-control textarea'], ['required' => true, 'placeholder' => ''] ) ) !!} 
                         @error('tags')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">SEO Meta Tags</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('meta_title ', 'Meta title ', ['class' => 'required']) !!} 
                        {!! Form::text('meta_title', old('meta_title'), array_merge(['class' => 'form-control'], ['required' => false, 'placeholder' => 'Meta title'] ) ) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('meta_description', 'Meta Description', ['class' => 'awesome']) !!}
                        {!! Form::textarea('meta_description', old('meta_description'), array_merge(['class' => 'form-control'], ['required' =>
                        false], ['rows' => 4] ) ) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('meta_image', 'Meta Image (300x300)', ['class' => 'required']) !!}
                        {!! Form::file('meta_image', array_merge(['class' => 'form-control'], ['required' => false] ) ) !!}
                    </div>
                </div>
                <!-- /.card-body -->
                   <div class="card-footer">
                        
                        {!! link_to_route('categories.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
                        {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success float-right']) !!}
                        {!! Form::close() !!}
                    </div>
            </div>
            <!-- /.card -->
        </div>
    </div>

    {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection
