@extends('admin.layouts._master')
@section('title', 'All Products')
@section('content')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card card-primary card-outline">
               <div class="card-header">
                  <a style="float: right;" href="{{ route('products.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Product</a>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Category</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @php $counter = 0; @endphp
                        @foreach($products as $key => $value)
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value->name }}</td>
                           <td>{{ $value->hasCategory['name'] }}</td>
                           @if($value->status == 1)
                           <td>
                              <a href="javascript:;" class="btn btn-success">Active</a>
                           </td>
                           @else
                           <td>
                              <a href="javascript:;" class="btn btn-danger">In-Active</a>
                           </td>
                           @endif
                           <td>
                              <a href="{{ route('products.edit', $value->id) }}" title="{{ trans('app.actions.edit') }}">
                              <i class="fas fa-edit text-warning"></i>
                              </a> | <a target="_blank" href="{{ route('product.detail', $value->slug) }}" title="{{ trans('app.actions.view') }}">
                              <i class="fas fa-eye text-success"></i>
                              </a> | <a href="javascript:;" onclick="commonDeleteAjax('{{ route('products.destroy', $value->id)}}');" title="Delete section">
                              <i class="fas fa-trash text-danger" aria-hidden="true"></i>
                              </a> 
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
@endsection