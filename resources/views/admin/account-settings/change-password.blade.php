@extends('admin.layouts._master')
@section('title', 'Change Password')
@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>
        </div>
        {!! Form::open(['route' => 'change-password.store', 'files' => false]) !!}
        <div class="card-body">
          <div class="form-group">
            {!! Form::label('current-password', 'Current Password', ['class' => 'required']) !!}
            {!! Form::password('current_password', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('current_password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
          <div class="form-group">
            {!! Form::label('password', 'New Password', ['class' => 'required']) !!}
            {!! Form::password('password', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
          <div class="form-group">
            {!! Form::label('password-confirm', 'Confirm Password', ['class' => 'required']) !!}
            {!! Form::password('password_confirmation', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
            @error('password_confirmation')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
               {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
               {!! link_to_route('admin.dashboard',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
               {!! Form::close() !!}
        </div>
      </div>
      <!-- /.card -->
    </div>
  </div>
</section>
<!-- /.content -->
@endsection