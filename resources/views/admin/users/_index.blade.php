@extends('admin.layouts._master')
@section('title', 'Customers')
@section('content')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card card-primary card-outline">
               <div class="card-header">
                  <a style="float: right;" href="{{ route('products.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Customer</a>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Email</th>
                           <th>Verified</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @php $counter = 0; @endphp
                        @foreach($customers as $key => $value)
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value->first_name }}</td>
                           <td>{{ $value->last_name }}</td>
                           <td>{{ $value->email }}</td>
                           @if($value->email_verified_at != NULL)
                           <td>
                              <a href="javascript:;" class="btn btn-success">
                                 {{ trans('Verified') }}
                              </a>
                           </td>
                           @else
                           <td>
                              <a href="javascript:;" class="btn btn-danger">
                                 {{ trans('Not Verified') }}
                              </a>
                           </td>
                           @endif
                        
                           <td>
                              <a href="{{ route('customers.show', $value->id) }}" title="{{ trans('app.actions.show') }}">
                              <i class="fas fa-eye text-success"></i>
                              </a>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
@endsection