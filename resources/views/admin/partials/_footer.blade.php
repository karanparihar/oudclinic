<footer class="main-footer text-center" style="color:#162c5e !important">
   <strong>Copyright &copy; {{ date('Y') }} <a href="{{ route('admin.dashboard') }}">{{ config('app.name', 'Laravel') }}</a>.</strong>
   All rights reserved.
   <!-- <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0-pre
      </div>
      -->  
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
   <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->