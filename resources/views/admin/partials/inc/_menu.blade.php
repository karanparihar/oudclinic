<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-item has-treeview">
        <a href="{{ route('admin.dashboard') }}" class="nav-link {{ areActiveRoutes(['admin.dashboard']) }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
                Dashboard
                <i class="fas {{ areActiveRoutesIcon(['admin.dashboard']) }} right"></i>
            </p>
        </a> 
    </li>
    <li class="nav-item has-treeview">
        <a href="{{ route('customers.index') }}" class="nav-link {{ areActiveRoutes(['customers.index', 'customers.show']) }}">
            <i class="nav-icon fas fa-users"></i>
            <p>Customers 
                <i class="fas {{ areActiveRoutesIcon(['customers.index', 'customers.show']) }} right"></i>
            </p>
        </a> 
    </li>
    <li class="nav-item has-treeview {{ areActiveRoutesMenuOpen(['products.index', 'products.create', 'products.edit', 'categories.index','categories.index','categories.create','categories.edit']) }}">
        <a href="#" class="nav-link {{ areActiveRoutes(['products.index', 'products.create', 'products.edit','categories.index','categories.create','categories.edit']) }}">
            <i class="nav-icon fas fa-shopping-cart"></i>
            <p>Products<i class="fas fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('products.create') }}" class="nav-link {{ areActiveRoutes(['products.create']) }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Add Product</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('products.index') }}" class="nav-link {{ areActiveRoutes(['products.index', 'products.edit']) }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>All Products</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('categories.index') }}" class="nav-link {{ areActiveRoutes(['categories.index','categories.create','categories.edit']) }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Category</p>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview {{ areActiveRoutesMenuOpen(['orders.index','orders.show']) }}">
        <a href="#" class="nav-link {{ areActiveRoutes(['orders.index','orders.show']) }}">
            <i class="nav-icon fas fa-shopping-cart"></i>
            <p>Sales<i class="fas fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('orders.index') }}" class="nav-link {{ areActiveRoutes(['orders.index', 'orders.show']) }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>All Orders</p>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview">
        <a href="{{ route('contacts.index') }}" class="nav-link {{ areActiveRoutes(['contacts.index', 'contacts.create', 'contacts.edit']) }}">
            <i class="nav-icon fas fa-envelope"></i>
            <p>Contact Request 
                <i class="fas {{ areActiveRoutesIcon(['contacts.index', 'contacts.create', 'contacts.edit']) }} right"></i>
            </p>
        </a> 
    </li>

    <li class="nav-item has-treeview">
        <a href="{{ route('testimonial.index') }}" class="nav-link {{ areActiveRoutes(['testimonial.index', 'testimonial.create', 'testimonial.edit']) }}">
            <i class="nav-icon fa fa-comments"></i>
            <p>Testimonial
                <i class="fas {{ areActiveRoutesIcon(['testimonial.index', 'testimonial.create', 'testimonial.edit']) }} right"></i>
            </p>
        </a> 
    </li>

    <li class="nav-item has-treeview {{ areActiveRoutesMenuOpen(['change-password.index']) }}">
        <a href="#" class="nav-link {{ areActiveRoutes(['change-password.index']) }}">
            <i class="nav-icon fas fa-cog"></i>
            <p>Account settings <i class="fas fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('change-password.index') }}" class="nav-link {{ areActiveRoutes(['change-password.index']) }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Change Password</p>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview {{ areActiveRoutesMenuOpen(['slider.index', 'slider.create', 'slider.edit']) }}">
        <a href="#" class="nav-link {{ areActiveRoutes(['slider.index','slider.create', 'slider.edit']) }}">
            <i class="nav-icon fas fa-plus"></i>
            <p>General settings <i class="fas fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('slider.index') }}" class="nav-link {{ areActiveRoutes(['slider.index','slider.create', 'slider.edit']) }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Slider</p>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview">
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
             document.getElementById('logout-form').submit();"
            class="nav-link">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
                Logout
            </p>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </li>
</ul>
