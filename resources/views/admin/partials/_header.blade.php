<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
   <!-- Left navbar links -->
   <ul class="navbar-nav">
     <li class="nav-item">
         <a class="nav-link text-white" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars" style="color:#4f5962;"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
         <a target="_blank" href="{{ route('home') }}" class="nav-link" target="_blank"><i class="fa fa-globe"></i>&nbsp;Visit Website</a>
      </li>
   </ul>
 
   <!-- Right navbar links -->
   <ul class="navbar-nav ml-auto">

   
   </ul>
</nav>
<!-- /.navbar -->