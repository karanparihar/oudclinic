@extends('admin.layouts._master')
@section('title', 'Dashboard')
@section('content')
<!-- Main content -->
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
               <span class="info-box-icon bg-info elevation-1"><i class="fas fa-flag"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Today Orders</span>
                  <span class="info-box-number">
                   {{ $todayOrder }}
                  </span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
               <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-shopping-cart"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Orders</span>
                  <span class="info-box-number">{{ $orders }}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <!-- fix for small devices only -->
         <div class="clearfix hidden-md-up"></div>
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
               <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Products</span>
                  <span class="info-box-number">{{ $products }}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
               <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Customers</span>
                  <span class="info-box-number">{{ $users }}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
      </div>
      <div class="row">
         <div class="col-12 col-sm-6 col-md-12">
            <div class="card">
               <div class="card-header border-transparent">
                  <h3 class="card-title">Latest Orders</h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body p-0">
                  <div class="table-responsive">
                     <table class="table m-0">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Order Code</th>
                              <th>Customer</th>
                              <th>Num. of Products</th>
                              <th>Amount</th>
                              <th>Payment Status</th>
                              <th>Delivery Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(!empty($latestOrders) )
                              @php $counter = 0; @endphp
                              @foreach($latestOrders as $key => $value)
                              <tr>
                                 <td>{{ ++$counter }}</td>
                                 <td>
                                    <a href="javascript:;">{{ $value->code }}</a>
                                 </td>
                                 <td>{{ $value->customer['first_name'] }} {{ $value->customer['last_name']}}</td>
                                 <td>{{ count($value->orderDetail) }}</td>
                                 <td>£{{ $value->grand_total }}</td>
                                 <td class="text-center">
                                    @if($value->payment_status == 'Paid')
                                     <span class="badge badge-success">{{ $value->payment_status}}</span>
                                    @else
                                    <span class="badge badge-danger">{{ $value->payment_status}}</span>
                                    @endif
                                 </td>
                                 <td class="text-center">
                                    @if($value->delivery_status == 'Pending')
                                     <span class="badge badge-danger">{{ $value->delivery_status}}</span>
                                    @elseif($value->delivery_status == 'Shipped')
                                    <span class="badge badge-success">{{ $value->delivery_status}}</span>
                                    @else
                                    <span class="badge badge-warning">{{ $value->delivery_status}}</span>
                                    @endif
                                 </td>
                              </tr>
                              @endforeach
                           @endif
                        
                        </tbody>
                     </table>
                  </div>
                  <!-- /.table-responsive -->
               </div>
               <!-- /.card-body -->
               <div class="card-footer clearfix">
                  <a href="{{ route('orders.index') }}" class="btn btn-sm btn-secondary float-right">View All Orders</a>
               </div>
               <!-- /.card-footer -->
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection