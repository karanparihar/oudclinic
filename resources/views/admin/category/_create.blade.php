@extends('admin.layouts._master')
@section('title', 'Add Category')
@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
        
        {!! Form::open(['route' => 'categories.store', 'files' => true]) !!}
        <div class="card-body">
	        <div class="form-group">
	            {!! Form::label('name', 'Name', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
	            {!! Form::text('name', old('name'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
	              @error('name')
	                  <span class="invalid-feedback" role="alert">
	                      <strong>{{ $message }}</strong>
	                  </span>
	              @enderror
	         </div>
         
            <div class="form-group">
              {!! Form::label('banner', 'Banner', ['class' => 'required']) !!}<sup class="text-danger text-bold">*</sup>
              {!! Form::file('banner', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('banner')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            </div>
            <div class="form-group">
              {!! Form::label('meta_title', 'Meta Title', ['class' => 'required']) !!}
              {!! Form::text('meta_title', old('meta_title'), array_merge(['class' => 'form-control'], ['required' => false] ) ) !!}
              @error('meta_title')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            </div>
            <div class="form-group">
	            {!! Form::label('meta_description', 'Meta Description', ['class' => 'awesome']) !!} 
	            {!! Form::textarea('meta_description', old('meta_description'), array_merge(['class' => 'form-control'], ['required' => false], ['rows' => 4] ) ) !!}
	             @error('meta_description')
	                  <span class="invalid-feedback" role="alert">
	                      <strong>{{ $message }}</strong>
	                  </span>
	              @enderror
          </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
            {!! link_to_route('categories.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
            {!! Form::close() !!}
        </div>

      </div>
      <!-- /.card -->
    </div>
   
  </div>

</section>
<!-- /.content -->

@endsection