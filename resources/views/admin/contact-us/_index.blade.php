@extends('admin.layouts._master')

@section('title', 'Contact Messages')

@section('content')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card card-primary card-outline">
             
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @if(!empty($messages))
                     @foreach($messages as $value)
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value->name }}</td>
                           <td>{{ $value->email }}</td>
                           <td>
                               <a href="{{ route('categories.edit', $value->id) }}" title="{{ trans('app.actions.edit') }}">
                                 <i class="fas fa-edit text-warning"></i>
                              </a>&nbsp;|&nbsp;

                              <a href="{{ route('categories.show', $value->id) }}" title="{{ trans('app.actions.view') }}">
                                 <i class="fas fa-eye text-success"></i>
                              </a> &nbsp;|&nbsp;

                              <a href="javascript:;" onclick="commonDeleteAjax('{{ route('categories.destroy', $value->id)}}');" title="Delete section">
                                 <i class="fas fa-trash text-danger" aria-hidden="true"></i>
                              </a> 

                           </td>
                        </tr>
                     @endforeach
                     @endif
                     </tbody>
                    
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>

@endsection