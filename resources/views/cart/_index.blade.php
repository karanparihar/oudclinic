@if(count($cart) > 0)
 <ul>
  @foreach($cart as $key => $cart)
    <li>
        <a class="cartbox-img" href="#">
          <img src="{{ displayBanner($cart->product['thumbnail_img']) }}" alt="{{ $cart->product_name }}" />
        </a>
        <div class="cartbox-info">
            <h4><a href="#">{{ $cart->product_name }} ({{ $cart->variant['size'] }}{{ $cart->variant['unit'] }})</a></h4>
            <p>{{ $cart->quantity }} x ${{ $cart->variant['price'] ?? $cart->product['purchase_price'] }}</p>
        </div>
        <a class="remove-product" href="javascript:;" onclick="removeCartItem({{$cart->id}})">
          <span class="fa fa-times text-danger" title="Remove item from cart."></span>
        </a>
    </li>
  @endforeach
  <li>
    <span class="cartbox-total-title">
        Total
    </span>
    <span class="cartbox-total-price">
        ${{ $totalPrice }}
    </span>
  </li>
</ul>
<a class="cartbox-checkout-btn" href="{{ route('checkout.index') }}">Checkout</a>
@else
  <p class="text-center">Your cart is empty.</p>
@endif


