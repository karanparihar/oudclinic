@extends('layouts.front')
@section('title', 'Checkout')
@section('content')
<!-- Checkout Section Begin -->
<section class="checkout spad">
    <div class="container">
        <div class="checkout_form">

            <form action="{{ route('payment.make') }}" method="POST" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="{{ Config::get('constants.stripe.key') }}" id="payment-form">
                @csrf
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <h3 class="checkout_title">Billing Address</h3>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Fist Name<span>*</span></p>
                                    <input type="text" name="first_name" value="{{ Auth::user()->first_name ?? '' }}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Last Name<span>*</span></p>
                                    <input type="text" name="last_name" value="{{ Auth::user()->last_name ?? '' }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="checkout_input">
                            <p>Address<span>*</span></p>
                            <input type="text" name="address" value="{{ Auth::user()->address ?? '' }}" required>
                        </div>
                        <div class="checkout_input">
                            <p>Address 2 (Optional)<span>*</span></p>
                            <input type="text" name="address2" value="{{ Auth::user()->address2 ?? '' }}">
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Town/City<span>*</span></p>
                                    <input type="text" name="city" value="{{ Auth::user()->city ?? '' }}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>State<span>*</span></p>
                                    <input type="text" name="state" value="{{ Auth::user()->state ?? '' }}" required>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Country<span>*</span></p>
                                    <input type="text" name="country" value="{{ Auth::user()->state ?? '' }}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Postcode / ZIP<span>*</span></p>
                                    <input type="text" name="zipcode" value="{{ Auth::user()->zipcode ?? ''}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Phone<span>*</span></p>
                                    <input type="text" value="{{ Auth::user()->phone  ?? '' }}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout_input">
                                    <p>Email<span>*</span></p>
                                    <input type="email" value="{{ Auth::user()->email ?? '' }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="same-address" name="shipping_address" value="1">
                              <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="save-info" name="save_address" value="1">
                              <label class="custom-control-label" for="save-info">Save this information for next time</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="checkout_order">
                            <h4 class="order_title">Your Cart ({{ count($cart) }})</h4>
                            <div class="checkout_order_products">Product <span>Total</span></div>
                            <ul class="checkout_total_products">
                                @if(!empty($cart))
                                   @foreach($cart as $key => $cart)
                                    <li>{{ $cart->product_name }} ({{ $cart->variant['size'] }}{{ $cart->variant['unit'] }}) <span>( {{ $cart->quantity }} )</span>
                                    <span>${{ $cart->variant['price'] ?? $cart->product['purchase_price'] }}</span>
                                    </li>
                                   @endforeach
                                @endif
                            </ul>
                            <ul class="checkout_total_all">
                                <li>Total <span>$ {{ $totalPrice }}</span></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="checkout_order">
                            <h4 class="order_title">Payment</h4>
                            <div class="row">
                                <input type="radio" name="payment_mode" value="COD">
                                <label>Cash on delivery</label></br>

                                <input type="radio" name="payment_mode" value="Stripe" checked>
                                <label>Debit or Credit Card</label>
                            </div>
                        <div class="stripe_form"> 
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label>
                                <input class='form-control' size='4' type='text'>
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> 
                                <input autocomplete='off' class='form-control card-number' size='20' type='text'>
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> 
                                <input autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> 
                                <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                 <label class='control-label'>Expiration Year</label>
                                 <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                again.
                                </div>
                            </div>
                        </div>
                            <button type="submit" class="site-btn">PAY NOW $( {{$totalPrice }} )</button>
                        </div>
                        <input type="hidden" name="payment" value="{{$totalPrice}}">
                    </div>
                </div>
            </form>
        
        </div>
    </div>
</section>
<!-- Checkout Section End -->
@endsection