<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="x-apple-disable-message-reformatting">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title></title>
      <style type="text/css">
         html,
         body {
         margin: 0 auto !important;
         padding: 0 !important;
         height: 100% !important;
         width: 100% !important;
         background-color: #f7f7f7 !important;
         }
         * {
         -ms-text-size-adjust: 100%;
         -webkit-text-size-adjust: 100%;
         } 
         table,
         td {
         mso-table-lspace: 0pt !important;
         mso-table-rspace: 0pt !important;
         }
         table { border-spacing: 0 !important; border-collapse: collapse !important;    }
         table table table { table-layout: auto; }
         img { -ms-interpolation-mode: bicubic; } 
         a[x-apple-data-detectors] {color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
         a { text-decoration: none; outline: none; }
         /*Gmail app fix*/ 
         div[style*="margin: 16px 0"] {
         margin: 0 !important;
         font-size: 100% !important;
         }
         /* samsung fix */
         #MessageViewBody, #MessageWebViewDiv{ width: 100% !important; }
         #MessageViewBody a {color: inherit; text-decoration: none; font-size: inherit; font-family: inherit; font-weight: inherit; line-height: inherit;}
         @media (max-width: 649px) {
         .o_col-full { max-width: 100% !important; }
         .o_col-half { max-width: 50% !important; }
         .o_hide-lg { display: inline-block !important; font-size: inherit !important; max-height: none !important; line-height: inherit !important; overflow: visible !important; width: auto !important; visibility: visible !important; }
         .o_hide-xs, .o_hide-xs.o_col_i { display: none !important; font-size: 0 !important; max-height: 0 !important; width: 0 !important; line-height: 0 !important; overflow: hidden !important; visibility: hidden !important; height: 0 !important; }
         .o_xs-center { text-align: center !important; }
         .o_xs-left { text-align: left !important; }
         .o_xs-right { text-align: left !important; }
         table.o_xs-left { margin-left: 0 !important; margin-right: auto !important; float: none !important; }
         table.o_xs-right { margin-left: auto !important; margin-right: 0 !important; float: none !important; }
         table.o_xs-center { margin-left: auto !important; margin-right: auto !important; float: none !important; }
         h1.o_heading { font-size: 32px !important; line-height: 41px !important; }
         h2.o_heading { font-size: 26px !important; line-height: 37px !important; }
         h3.o_heading { font-size: 20px !important; line-height: 30px !important; }
         .o_xs-py-md { padding-top: 24px !important; padding-bottom: 24px !important; }
         .o_xs-pt-xs { padding-top: 8px !important; }
         .o_xs-pb-xs { padding-bottom: 8px !important; }
         .o_xs-pt0  { padding-top: 0px !important; }
         .o_xs-pb0  { padding-bottom: 0px !important; }
         .o_xs-w100 { width: 100% !important;}
         }
         @media screen {
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 400;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/poppins/v15/pxiEyp8kv8JHgFVrJJfecnFHGPc.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 700;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLCz7Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 900;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLBT5Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         .o_sans, .o_heading { font-family: "Poppins", Arial, Poppins, sans-serif !important; }
         .o_heading, strong, b { font-weight: 700 !important; }
         a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; }
         }
      </style>
      <!--[if mso]>
      <style>
         table { border-collapse: collapse; }
         .o_col { float: left; }
      </style>
      <xml>
         <o:OfficeDocumentSettings>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
   </head>
   <body class="o_body" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #f7f7f7;">
      <!-- preview-text -->
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
         <tbody>
            <tr>
               <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
         </tbody>
      </table>
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
         <tbody>
            <tr>
               <td class="o_px-xs o_pt-lg o_xs-pt-xs" align="center" style="background-color: #f7f7f7;padding-left: 8px;padding-right: 8px;padding-top: 32px;">
                  <!--[if mso]>
                  <table width="632" cellspacing="0" cellpadding="0" border="0" role="presentation">
                     <tbody>
                        <tr>
                           <td>
                              <![endif]-->
                              <table class="o_block" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 632px;margin: 0 auto;">
                                 <tbody>
                                    <tr>
                                       <td class="o_px o_py-md o_br-t o_sans o_text" align="center" style="font-family: Poppins, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;background-color: #ffffff;border-radius: 12px 12px 0px 0px;padding-left: 16px;padding-right: 16px;padding-top: 32px;padding-bottom: 32px;">
                                          <p style="margin-top: 0px;margin-bottom: 0px;">
                                             <a href="#" style="text-decoration: none;outline: none;color: #126de5;">
                                             <img src="{{ asset('images/emails/logo.png') }}" width="175" height="38" alt="" style="max-width: 175px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none;"></a>
                                          </p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <!--[if mso]>
                           </td>
                        </tr>
                  </table>
                  <![endif]-->
               </td>
            </tr>
            <tr>
               <td align="center" style="background-color: #f7f7f7;padding-left: 8px;padding-right: 8px;">
                  <!--[if mso]>
                  <table width="632" cellspacing="0" cellpadding="0" border="0" role="presentation">
                     <tbody>
                        <tr>
                           <td>
                              <![endif]-->
                              <table  width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 632px;margin: 0 auto;">
                                 <tbody>
                                    <tr>
                                       <td class="o_px-md o_py-xl o_xs-py-md o_sans" align="center" style="font-family: Poppins, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;background-color: #ffffff;color: #303030;padding-left: 24px;padding-right: 24px;padding-top: 8px;padding-bottom: 16px;">
                                          <p class="o_mb-xxs" style="font-family: Poppins, Arial, sans-serif;font-weight: 500;margin-top: 0px;margin-bottom: 4px;color: #076982;font-size: 15px;line-height: 30px;"> 
                                             <br>{{ $data['message'] }}
                                          </p>
                                          <p class="o_mb-xxs" style="font-family: Poppins, Arial, sans-serif;font-weight: 500;margin-top: 0px;margin-bottom: 4px;color: #076982;font-size: 15px;line-height: 30px;"> 
                                             <br>
                                          </p>
                                          <h3 style="text-align: center;">Event Notification - {{ $data['program'] ?? '' }}</h3>
                                          <h4 style="text-align: center;">Date - {{ date('Y-m-d') }}</h4>

                                          @if(!empty($data['notifications']['files']) && isset($data['notifications']['files']))
                                          
                                          <h4>Uploaded Files:</h4>
                                          
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             <tr>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">File</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Description</th>
                                             </tr>
                                             @foreach($data['notifications']['files'] as $file)
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   <a href="{{ url('/user/file/download/' . $file['id']) }}">
                                                   {{ $file['filename'] ?? '' }} 
                                                  </a>
                                                 </td>
                                                 <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $file['description'] ?? '' }}
                                                 </td>
                                             </tr>
                                             @endforeach
                                          </table>
                                          @endif

                                          @if(!empty($data['notifications']['resource_library']) && isset($data['notifications']['resource_library']))
                                         
                                          <h4>Resource Library:</h4>
                                         
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             <tr>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">File</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Description</th>
                                             </tr>
                                             @foreach($data['notifications']['resource_library'] as $file)
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   <a href="{{ url('/user/file/download/' . $file['id']) }}"> 
                                                      {{ $file['filename'] ?? '' }}
                                                   </a>
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                     {{ $file['description'] ?? '' }}
                                                </td>
                                             </tr>
                                             @endforeach
                                          </table>
                                          @endif

                                          @if(!empty($data['notifications']['task']) && isset($data['notifications']['task']))
                                          
                                          <h4>Tasks:</h4>
                                          
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             <tr>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Task</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Description</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Start Date</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">End Date</th>
                                             </tr>
                                             @foreach($data['notifications']['task'] as $task)
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $task['name'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                    {{ $task['description'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $task['start_date'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $task['end_date'] ?? '' }}
                                                </td>
                                             </tr>
                                             @endforeach
                                          </table>
                                          @endif   

                                          @if(!empty($data['notifications']['invoices_or_plan']) && isset($data['notifications']['invoices_or_plan']))
                                          <h4>Plans:</h4>
                                          
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             <tr>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Name</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Amount</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Invoice Number</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Description</th>
                                             </tr>
                                             @foreach($data['notifications']['invoices_or_plan'] as $value)
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['name'] ?? ''}}
                                               </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['amount'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['invoice_number'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['description'] ?? '' }}
                                                </td>
                                             </tr>
                                             @endforeach
                                          </table>
                                          @endif   

                                          @if(!empty($data['notifications']['methodology']) && isset($data['notifications']['methodology']))
                                          <h4>Methodologies:</h4>
                                         
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             <tr>
                                               <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Title</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Content</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Icon</th>
                                             </tr>
                                             @foreach($data['notifications']['methodology'] as $value)
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['title'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['content'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['icon'] ?? '' }}
                                                </td>
                                             </tr>
                                             @endforeach
                                          </table>
                                          @endif 

                                          @if(!empty($data['whiteboard']) && isset($data['whiteboard']))
                                          <h4>WhiteBoard:</h4>
                                          
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                  {{ $data['whiteboard'] ?? '' }}
                                               </td>
                                             </tr>
                                            
                                          </table>
                                          @endif 

                                          @if(!empty($data['notifications']['impactInventory']) && isset($data['notifications']['impactInventory']))
                                          <h4>Impact Inventory:</h4>
                                          
                                          <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                                             <tr>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Outcome</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Target Measures</th>
                                                <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Description</th>
                                             </tr>
                                             @foreach($data['notifications']['impactInventory'] as $value)
                                             <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                    {{ $value['outcome'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['target_measures'] ?? '' }}
                                                </td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
                                                   {{ $value['description'] ?? '' }}
                                                </td>
                                             </tr>
                                             @endforeach
                                          </table>
                                          @endif 

                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="o_px-md o_py-xl o_xs-py-md o_sans" align="center" style="font-family: Poppins, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 18px;line-height: 24px;background-color: #ffffff;color: #076982;padding-left: 24px;padding-right: 24px;padding-top: 8px;padding-bottom: 16px;">
                                          <table align="center" cellspacing="0" cellpadding="0" border="0" role="presentation">
                                             <tbody>
                                                <tr>
                                                   <td width="300" class="o_btn o_bg-success o_br o_heading o_text" align="center" style="font-family: Poppins, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 0px;font-size: 18px;line-height: 24px;mso-padding-alt: 12px 24px;background-color: #ffffff;">
                                                      <br />
                                                      <a class="o_text-white" href="{{ URL::to('/') }}" style="text-decoration: none;outline: none;color: #076982;display: block;padding: 12px 24px;mso-text-raise: 3px;border-radius: 12px;
                                                         border: 2px solid #076982;">Login</a>
                                                      <br /><br />
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <!--[if mso]>
                           </td>
                        </tr>
                  </table>
                  <![endif]-->
               </td>
            </tr>
         </tbody>
      </table>
      <!--[if mso]></td></tr></table><![endif]-->
      <!-- </td>
         </tr>  -->
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
         <tbody>
            <tr>
               <td class="o_bg-light o_px-xs o_pb-lg o_xs-pb-xs" align="center" style="background-color: #f7f7f7;padding-left: 8px;padding-right: 8px;padding-bottom: 32px;">
                  <!--[if mso]>
                  <table width="632" cellspacing="0" cellpadding="0" border="0" role="presentation">
                     <tbody>
                        <tr>
                           <td>
                              <![endif]-->
                              <table class="o_block" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 632px;margin: 0 auto;">
                                 <tbody>
                                    <tr>
                                       <td class="o_re o_bg-white o_px o_pb-lg o_bt-light o_br-b" align="center" style="font-size: 0;vertical-align: top;background-color: #15808c;  padding-left: 16px;padding-right: 16px;padding-bottom: 32px;">
                                          <!--[if mso]>
                                          <table cellspacing="0" cellpadding="0" border="0" role="presentation">
                                             <tbody>
                                                <tr>
                                                   <td width="200" align="left" valign="top" style="padding:0px 8px;">
                                                      <![endif]-->
                                                      <div class="o_col o_col-2" style="display: inline-block;vertical-align: top;width: 100%;max-width: 200px;">
                                                         <div style="font-size: 8px; line-height: 8px; height: 8px;">&nbsp; </div>
                                                         <div class="o_px-xs o_sans o_text-xs o_text-light o_right o_xs-center" style="font-family: Poppins, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;color: #82899a;text-align: left;padding-left: 8px;padding-right: 8px;">
                                                            <p style="margin-top: 0px;margin-bottom: 0px;">
                                                               <a class="o_text-light" href="http://www.bingoimpact.com" style="text-decoration: none;outline: none;color: #82899a;"><img src="{{  asset('images/emails/footerlogo.png') }}" width="166" height="56" alt="" style="max-width: 166px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none;"></a>
                                                            </p>
                                                         </div>
                                                      </div>
                                                      <!--[if mso]>
                                                   </td>
                                                   <td width="400" align="right" valign="top" style="padding:0px 8px;">
                                                      <![endif]-->                     
                                                      <div class="o_col o_col-4" style="display: inline-block;vertical-align: top;width: 100%;max-width: 400px;">
                                                         <div style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; </div>
                                                         <div class="o_px-xs o_sans o_text-xs o_text-light o_left o_xs-center" style="font-family: Poppins, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;color: #ffffff;text-align: right;padding-left: 8px;padding-right: 8px;">
                                                            <p style="margin-top: 0px;margin-bottom: 0px;">
                                                               <a href="https://www.linkedin.com/company/bingo-impact/"><img src="{{  asset('images/emails/in.png') }}" width="24" height="24" alt="fb"></a><span> &nbsp;</span>
                                                               <a href="https://twitter.com/bingoimpact"><img src="{{  asset('images/emails/twitter.png') }}" width="24" height="24" alt="tw"></a> 
                                                            </p>
                                                         </div>
                                                      </div>
                                                      <!--[if mso]>
                                                   </td>
                                                </tr>
                                          </table>
                                          <![endif]-->
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="o_bg-white o_px-md o_pb-lg o_br-b o_sans o_text-xs o_text-light" align="center" style="font-family: Poppins, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;background-color: #15808c;color: #ffffff;border-radius: 0px 0px 12px 12px;padding-left: 24px;padding-right: 24px;padding-bottom: 32px;">
                                          <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                                             Copyright © 2021  Bingo Impact Management Consulting Inc. <br>
                                             All rights reserved.<br><br>
                                             <!--Want to change how you receive these emails?<br>
                                                You can <a href="#" style="text-decoration: underline;outline: none;color: #ffffff;">update your preferences</a> or
                                                <a href="#" style="text-decoration: underline;outline: none;color: #ffffff;">unsubscribe from this list.</a>
                                                -->
                                          </p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              </div>
                              <!--[if mso]>
                           </td>
                        </tr>
                  </table>
                  <![endif]-->
                  <div class="o_hide-xs" style="font-size: 64px; line-height: 64px; height: 64px;">&nbsp; </div>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>