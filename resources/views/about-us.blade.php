@extends('layouts.front')
@section('title', 'About')
@section('content')

  <!-- about section -->
  <section class="about spad">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12">
            <!-- Section Heading-->
            <div class="section-heading text-center">
              <h3>About Us</h3>
            </div>
          </div>
        </div>
        <div class="row mrgn-top-50">
          <div class="col-md-6 col-lg-6">
            <img src="{{ asset('assets/site/img/about.jpg') }}">
          </div>
          <div class="col-md-6 col-lg-6">
            <div class="about_item">
              <h4>Who We Are ?</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
            </div>
            <div class="about_item">
              <h4>Who We Do ?</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
            </div>
            <div class="about_item">
              <h4>Why Choose Us</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
            </div>
          </div> 
          </div>
        </div>
      </div>
  </section>
  <!-- / about section -->
   <!-- Testimonial -->
  <section id="testimonial"> 
    <div id="myCarouseltesti" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarouseltesti" data-slide-to="0" class="active"></li>
        <li data-target="#myCarouseltesti" data-slide-to="1"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <div class="testimonial-single">
            <img class="testimonial-img" src="{{ asset('assets/site/img/thumbnail.jpg') }}" alt="testimonial img">
              <span class="fa fa-quote-left testimonial-quote"></span>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt distinctio omnis possimus, facere, quidem qui!consectetur adipisicing elit. Sunt distinctio omnis possimus, facere, quidem qui.</p>
              <div class="testimonial-info">
                <p>KEVIN MEYER</p>
              </div>
          </div>
        </div>

        <div class="item">
          <div class="testimonial-single">
            <img class="testimonial-img" src="{{ asset('assets/site/img/thumbnail.jpg') }}" alt="testimonial img">
              <span class="fa fa-quote-left testimonial-quote"></span>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt distinctio omnis possimus, facere, quidem qui!consectetur adipisicing elit. Sunt distinctio omnis possimus, facere, quidem qui.</p>
              <div class="testimonial-info">
                <p>KEVIN MEYER</p>
              </div>
          </div>
        </div>
      </div>
    </div> 
  </section>
  <!-- / Testimonial -->

@endsection