@extends('layouts.front')
@section('title', 'Contact')
@section('content')

 <!-- Contact Section Begin -->
  <section class="contact spad">
      <div class="container">
          <div class="row">
              <div class="col-lg-6 col-md-6">
                  <div class="contact_text">
                      <div class="section-title">
                          <h2>Contact Us</h2>
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                      </div>
                      <ul>
                          <li>
                              <h4>America</h4>
                              <p>25 Astor Road, NY 12303, USA <br />+1 212-123-4589</p>
                          </li>
                          <li>
                              <h4>France</h4>
                              <p>109 Avenue Leon, 63 Clermont-Ferrand <br />+12 345-423-9893</p>
                          </li>
                      </ul>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6">
                  <div class="contact_form">
                      <form action="{{ route('contact-us.store') }}" method="POST">
                        @csrf
                          
                          <div class="row">
                              <div class="col-lg-6">
                                  <input type="text" name="name" value="{{ old('name') }}" placeholder="Name" required>

                                    @error('name')
                                       <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                       </span>
                                    @enderror

                              </div>
                              <div class="col-lg-6">
                                  <input type="email" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                    @error('email')
                                       <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                       </span>
                                    @enderror

                              </div>
                              <div class="col-lg-12">
                                  <textarea placeholder="Message" name="message" required>{{ old('message') }}</textarea>

                                    @error('message')
                                       <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                       </span>
                                    @enderror

                                  {!! RecaptchaV3::initJs() !!}
                                  {!! RecaptchaV3::field('contact-us') !!}
                                 
                                 <button type="submit" class="site-btn">Send Message</button>

                              </div>
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- Contact Section End -->
  <!-- Map Begin -->
  <div class="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d111551.9926412813!2d-90.27317134641879!3d38.606612219170856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2sbd!4v1597926938024!5m2!1sen!2sbd" height="500" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
  <!-- Map End -->

@endsection