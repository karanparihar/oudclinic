@extends('layouts.front') @section('title', $product->name) @section('content')
<!-- Shop Details Section Begin -->
<section id="product-details">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-details-area">
                    <div class="product-details-content">
                        <div class="row">
                            <div class="product-view-slider col-md-6 col-sm-6 col-xs-12">
                                <div id="demo-1" class="simpleLens-gallery-container">
                                    <div class="simpleLens-thumbnails-container">
                                        
                                        @if(!empty($product->hasImages)) 
                                            @foreach($product->hasImages as $key => $value)    
                                                <a data-big-image="{{ displayBanner($value->image) }}" data-lens-image="{{ displayBanner($value->image) }}" class="simpleLens-thumbnail-wrapper" href="#">
                                                 <img src="{{ displayBanner($value->image) }}"/>
                                                </a>
                                            @endforeach 
                                        @endif
                                    </div>

                                    <div class="simpleLens-container">
                                        <div class="simpleLens-big-image-container">
                                            <a data-lens-image="{{ displayBanner($product->thumbnail_img) }}" class="simpleLens-lens-image">
                                                <img src="{{ displayBanner($product->thumbnail_img) }}" class="simpleLens-big-image"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="product-view-content">
                                    <h3>{{ $product->name }}</h3>
                                    <div class="price-block">
                                        @if(count($product->variant) == 0)
                                            <span class="product-view-price">
                                                {{ trans('app.currency') }}{{ $product->purchase_price }}
                                            </span>
                                        @else
                                        <span class="product-view-price">{{ trans('app.currency') }}<span class="product-price">{{ $product->variant[0]->price }}</span>
                                        </span>
                                        @endif
                                        <p class="product-avilability">Avilability: <span class="text-success"><i class="fa fa-check"></i>&nbsp;In stock</span></p>
                                    </div>
                                    <h4>Highlights:</h4> <p>{!! $product->tags !!}</p>
                                    <div class="product_details_option">
                                        
                                        <h4>Size</h4>
                                        @if(count($product->variant) != 0)
                                        <div class="prod-view-size">
                                            @foreach($product->variant as $key => $value)
                                            <a href="javascript:;" id="{{$value->id}}-variant" onclick="variation( {{$product->id}},{{$value->id }})" class="@if($key ==0) active @endif">
                                                {{ $value->size }} {{ $value->unit }}
                                            </a>
                                            @endforeach
                                        </div>
                                        @else
                                        <div class="prod-view-size">
                                            <a href="javascript:;" id="" class="active">
                                                {{ $product->product_size }}
                                            </a>
                                        </div>
                                        @endif
                                        @if(count($product->variant) != 0)
                                           <input type="hidden" id="product_variant" name="product_variant" value="{{ $product->variant[0]->id }}">
                                        @endif
                                        

                                        <div class="prod-quantity">
                                            <h4>Quantity</h4>
                                            <form action="#">
                                                <select name="quantity" id="prodcut-qty">
                                                    <option value="1">1</option>
                                                    <option value="1">2</option>
                                                    <option value="2">3</option>
                                                    <option value="3">4</option>
                                                    <option value="4">5</option>
                                                    <option value="5">6</option>
                                                </select>
                                            </form>
                                            @if(Auth::guest())
                                            <a class="add-to-cart-btn" data-toggle="modal" data-target="#myModal">
                                                Add To Cart
                                            </a>
                                            @else
                                            <a class="add-to-cart-btn" href="javascript:;" onclick="addToCart({{$product->id}});">Add To Cart</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-details-bottom">
                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active"><a href="#description" data-toggle="tab">Description</a></li>
                            <li><a href="#review" data-toggle="tab">Reviews</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="description">
                                <p>{!! $product->product_description !!}</p>
                            </div>
                            <div class="tab-pane fade" id="review">
                                <div class="product-review-area">
                                    <h4>2 Reviews</h4>
                                    <ul class="review-nav">
                                        <li>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                        <img
                                                            class="media-object"
                                                            src="{{ asset(
                              'assets/site/img/thumbnail.jpg') }}"
                                                            alt="image"
                                                        />
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><strong>Customer Name</strong> - <span>Dec 26, 2021</span></h4>
                                                    <div class="product-rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    </div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                        <img class="media-object" src="{{ asset('assets/site/img/thumbnail.jpg') }}" alt="image" />
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><strong>Customer Name</strong> - <span>Dec 26, 2021</span></h4>
                                                    <div class="product-rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    </div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <h4>Add a review</h4>
                                    <div class="your-rating">
                                        <p>Your Rating</p>
                                        <a href="#"><span class="fa fa-star-o"></span></a>
                                        <a href="#"><span class="fa fa-star-o"></span></a>
                                        <a href="#"><span class="fa fa-star-o"></span></a>
                                        <a href="#"><span class="fa fa-star-o"></span></a>
                                        <a href="#"><span class="fa fa-star-o"></span></a>
                                    </div>
                                    <!-- review form -->
                                    <form action="" class="review-form">
                                        <div>
                                            <label for="message">Your Review</label>
                                            <textarea rows="3" id="message"></textarea>
                                        </div>
                                        <div>
                                            <label for="name">Name</label>
                                            <input type="text" id="name" placeholder="Name" />
                                        </div>
                                        <div>
                                            <label for="email">Email</label>
                                            <input type="email" id="email" placeholder="example@gmail.com" />
                                        </div>

                                        <button type="submit" class="btn btn-default review-submit">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Related product -->
                    <div class="product-related-item">
                        <h3>Related Products</h3>
                        <div class="row">
                            @if(!empty($relatedProducts)) @foreach($relatedProducts as $key => $value)
                            <div class="col-xs-6 col-sm-3 col-lg-3 product-catg">
                                <figure>
                                    <div class="relative">
                                        <a class="product-img" href="{{ route('product.detail', $value->slug) }}">
                                            <img src="{{ displayBanner($value->thumbnail_img) }}" alt="{{ $value->slug }}" style="height: 250px; width: 300px;" />
                                        </a>
                                        <a class="add-card-btn" href="{{ route('product.detail', $value->slug) }}">Buy Now</a>
                                    </div>
                                    <figcaption>
                                        <h4 class="product-title"><a href="{{ route('product.detail', $value->slug) }}">{{ $product->name }}</a></h4>
                                        <span class="product-price">{{ trans('app.currency') }}{{ $product->purchase_price}}</span>
                                    </figcaption>
                                </figure>
                            </div>
                            @endforeach 
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Shop Details Section End -->

@endsection
