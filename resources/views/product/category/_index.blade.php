@extends('layouts.front') @section('title', $category->name) @section('content')

 <!-- Products section -->
  <section id="product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="product-area">
              <div class="product-inner">
                <h2 class="section-title">All Products</h2>
                <div class="row">
                  @if(!empty($products))
                    @foreach($products as $key => $value)
                      <div class="col-xs-6 col-sm-3 col-lg-3 product-catg">
                        <figure>
                          <div class="relative">
                            <a class="product-img" href="{{ route('product.detail', Str::slug($value->name) ) }}">
                              <img src="{{ displayBanner($value->thumbnail_img) }}" alt="{{ $value->slug }}" style="height:250px; width:300px;">
                            </a>
                            <a class="add-card-btn"href="#">
                              <span class="fa fa-shopping-cart"></span>Add To Cart
                            </a>
                          </div>
                          <figcaption>
                            <h4 class="product-title">
                              <a href="{{ route('product.detail', $value->slug ) }}" alt="{{ $value->slug }}">{{ $value->name }}</a>
                            </h4>
                            <span class="product-price">${{ $value->purchase_price }}</span>
                          </figcaption>
                        </figure>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>       
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- Category section -->
  <section id="category">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="product-area">
              <div class="product-inner">
                <h2 class="section-title">All Categories</h2>
                <div class="carousel row" data-flickity='{"cellAlign": "left", "lazyLoad": true, "wrapAround": true, "pageDots": false, "initialIndex": 2, "autoPlay": false }'>
                  @if(!empty($categories))
                    @foreach($categories as $key => $value)
                      <div class="carousel-cell"> 
                        <figure>
                          <div class="relative">
                            <a class="product-img" href="#">
                              <img src="{{ displayBanner($value->banner) }}" alt="{{ $value->slug }}" style="height:250px; width:300px;">
                          </a>
                          </div>
                          <figcaption>
                            <h4 class="product-title text-center"><a href="#">{{ $value->name }}</a></h4>
                          </figcaption>
                        </figure>    
                      </div>
                    @endforeach
                  @endif
                 
                </div>
              </div>
            </div>       
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection