@extends('layouts.front')
@section('title', 'Services')
@section('content')

<!-- Services section -->
  <section class="service_plan_area spad">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12">
            <!-- Section Heading-->
            <div class="section-heading text-center">
              <h3>Our Services</h3>
              <p>Select your favourite service from below boxes</p>
              <div class="line"></div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <!-- Single Price Plan Area-->
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="single_service_plan" >
              <div class="title">
                <h3>Service Name</h3>
                <p>Start a trial</p>
                <div class="line"></div>
              </div>
              <div class="price">
                <h4>$0</h4>
              </div>
              <div class="description">
                <p><i class="fa fa-check"></i>Duration: 7days</p>
                <p><i class="fa fa-check"></i>10 Features</p>
                <p><i class="fa fa-times"></i>No Hidden Fees</p>
                <p><i class="fa fa-times"></i>100+ Video Tuts</p>
                <p><i class="fa fa-times"></i>No Tools</p>
              </div>
              <div class="button"><a class="browse-btn" href="#">Get Started</a></div>
            </div>
          </div>
          <!-- Single Price Plan Area-->
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="single_service_plan active">
              <!-- Side Shape-->
              <div class="side-shape"><img src="{{ asset('assets/site/img/popular-service.png') }}" alt=""></div>
              <div class="title"><span>Selected</span>
                <h3>Service Name</h3>
                <p>For Small Organization</p>
                <div class="line"></div>
              </div>
              <div class="price">
                <h4>$9.99</h4>
              </div>
              <div class="description">
                <p><i class="fa fa-check"></i>Duration: 3 Month</p>
                <p><i class="fa fa-check"></i>50 Features</p>
                <p><i class="fa fa-check"></i>No Hidden Fees</p>
                <p><i class="fa fa-check"></i>150+ Video Tuts</p>
                <p><i class="fa fa-times"></i>5 Tools</p>
              </div>
              <div class="button"><a class="browse-btn" href="#">Get Started</a></div>
            </div>
          </div>
          <!-- Single Price Plan Area-->
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="single_service_plan">
              <div class="title">
                <h3>Service Name</h3>
                <p>Unlimited Possibilities</p>
                <div class="line"></div>
              </div>
              <div class="price">
                <h4>$49.99</h4>
              </div>
              <div class="description">
                <p><i class="fa fa-check"></i>Duration: 1 year</p>
                <p><i class="fa fa-check"></i>Unlimited Features</p>
                <p><i class="fa fa-check"></i>No Hidden Fees</p>
                <p><i class="fa fa-check"></i>Unlimited Video Tuts</p>
                <p><i class="fa fa-check"></i>Unlimited Tools</p>
              </div>
              <div class="button"><a class="browse-btn" href="#">Get Started</a></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- / Services section -->

@endsection