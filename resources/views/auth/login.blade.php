@extends('layouts.front')
@section('title', 'Login')
@section('content')
  <!-- Login section -->
  <section id="myaccount">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="myaccount-login">
            <h4>{{ __('Login') }}</h4>
            
            <form  method="POST" action="{{ route('login') }}" class="login-form">
              @csrf

              <label for="">{{ __('E-Mail Address') }}<span>*</span></label>
              <input id="email" type="email" placeholder="Email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
              
              @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror

              <label for="">{{ __('Password') }}<span>*</span></label>
              <input id="password" type="password" placeholder="Password" class="@error('password') is-invalid @enderror"  name="password" required autocomplete="current-password">
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror

              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>

              <button type="submit" class="browse-btn">Sign In </button>
              <p class="lost-password">
                <a href="{{ route('password.request') }}">Lost your password?</a>
                <a href="{{ route('register') }}">Create Account</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Login section -->
@endsection
