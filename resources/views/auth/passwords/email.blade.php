@extends('layouts.front')

@section('content')
  <!-- Login section -->
  <section id="myaccount">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="myaccount-login">
            <h4>{{ __('Reset Password') }}</h4>
            
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}" class="login-form">
              @csrf
              
              <label for="">{{ __('E-Mail Address') }}<span>*</span></label>
              <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

              @error('email')
                 <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
             
              <button type="submit" class="browse-btn"> {{ __('Send Password Reset Link') }}</button>
              <p class="lost-password">
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Create Account</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Login section -->
@endsection
