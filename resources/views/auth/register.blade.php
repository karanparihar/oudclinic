@extends('layouts.front')
@section('title', 'Register')
@section('content')
<!-- Login section -->
  <section id="myaccount">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="myaccount-login">
            <h4>{{ __('Register') }}</h4>
            
            <form method="POST" action="{{ route('register') }}" class="register-form">
              @csrf
              <div class="form-group">
                <label for="">{{ __('First Name') }}<span>*</span></label>
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                
                @error('first_name')
                   <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                   </span>
                @enderror
              </div>

              <div class="form-group">
                <label for="">{{ __('Last Name') }}<span>*</span></label>
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                @error('last_name')
                   <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                   </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="">{{ __('E-Mail Address') }}<span>*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                   <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                   </span>
                @enderror
              </div>
              
              <div class="form-group">
                <label for="">{{ __('Password') }}<span>*</span></label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="">{{ __('Confirm Password') }}<span>*</span></label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              </div>
              
              <button type="submit" class="browse-btn">{{ __('Register') }}</button>
            </form>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Login section -->
@endsection
