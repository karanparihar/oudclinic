<footer id="footer">
    <!-- footer bottom -->
    <div class="footer-top">
     <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="footer-top-area">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                  <h3>Main Menu</h3>
                  <ul class="footer-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Our Services</a></li>
                    <li><a href="#">Our Products</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                  <div class="footer-widget">
                    <h3>Knowledge Base</h3>
                    <ul class="footer-nav">
                      <li><a href="#">Delivery</a></li>
                      <li><a href="#">Returns</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Discount</a></li>
                      <li><a href="#">Special Offer</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                  <div class="footer-widget">
                    <h3>Useful Links</h3>
                    <ul class="footer-nav">
                      <li><a href="#">Site Map</a></li>
                      <li><a href="#">Suppliers</a></li>
                      <li><a href="#">FAQ</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                  <div class="footer-widget">
                    <h3>Contact Us</h3>
                    <address>
                      <p> 25 Astor Road, NY 12303, USA</p>
                      <p><span class="fa fa-phone"></span> +1 212-123-4589</p>
                      <p><span class="fa fa-envelope"></span> oudclinic@gmail.com</p>
                    </address>
                    <div class="footer-social">
                      <a href="#"><span class="fa fa-facebook"></span></a>
                      <a href="#"><span class="fa fa-twitter"></span></a>
                      <a href="#"><span class="fa fa-google-plus"></span></a>
                      <a href="#"><span class="fa fa-youtube"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>
    </div>
    <!-- footer-bottom -->
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="footer-bottom-area">
            <p>© 2021 Oudclinic All Rights Reserved</p>
            <div class="footer-payment">
              <span class="fa fa-cc-mastercard"></span>
              <span class="fa fa-cc-visa"></span>
              <span class="fa fa-paypal"></span>
              <span class="fa fa-cc-discover"></span>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </footer>

  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Go to cart</h4>
      </div>
      <div class="modal-body">
        <h2>Before you can do that...</h2>
        <p>Sign in or register with your email address</p>
         <a class="add-card-btn" route="{{ route('login') }}">Sign In</a>
              <p class="lost-password">
                <a href="{{ route('password.request') }}">Lost your password?</a>
                <a href="{{ route('register') }}">Create Account</a>
              </p>
      </div>
    </div>
  </div>
</div>