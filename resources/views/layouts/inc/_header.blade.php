<!-- Start header section -->
  <header id="header">
    <!-- start header top  -->
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="header-top-area">
              <div class="text-center">
                <span>Free shipping on U.S. orders over $100</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="header-bottom-area">
              <!-- logo  -->
              <div class="logo">
                <a href="{{ route('home') }}"><img src="{{ asset('assets/site/img/logo.jpg') }}" alt="logo img"></a>
              </div>
              <!-- / logo  -->
              <div class="header-bottom-right">
                <!-- search box -->
                <div class="search-box hidden-xs">
                  <form action="">
                    <input type="text" name="" id="" placeholder="Search here...">
                    <button type="submit"><span class="fa fa-search"></span></button>
                  </form>
                </div>
                <!-- / search box -->      
                <!-- cart & Login box -->
                <div class="login">
                  @if(Auth::guest())
                  <a class="login-link" href="{{ route('login') }}">
                    <img src="{{ asset('assets/site/img/avatar.svg') }}">
                  </a>
                  @else
                    @if(Auth::user()->role == 'Admin')
                    <a class="" href="{{ route('admin.dashboard') }}">
                        {{ trans('My Panel') }}
                    </a>
                   
                    @else
                    <a class="" href="{{ route('customer.dashboard') }}">
                        {{ trans('My Panel') }}
                    </a>
                    @endif
                     <a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                        {{ trans('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                  
                  @endif
                </div>
                @php $cart = []; $totalPrice = 0; @endphp

                @if(Auth::check() == true)
                    @php 
                      $cart = \App\Models\Cart::where(['user_id' => Auth::user()->id])->get();
                      if(count($cart) > 0) {
                        $totalPrice = 0;
                        foreach($cart as $key => $value) {
                            $totalPrice = $totalPrice + ($value->price * $value->quantity);
                        }
                      } 
                    @endphp
                @endif
                <div class="cartbox">
                    <a class="cart-link" href="#">
                      <img src="{{ asset('assets/site/img/shopping-cart.svg') }}">
                      <span class="cart-badge">{{ count($cart) ?? 0 }}</span>
                    </a>
                    <div class="cartbox-summary">
                      @include('cart._index', compact('cart', 'totalPrice'))
                    </div>
                </div>
                <!-- / cart & Login box  -->
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header bottom  -->
  </header>
  <!-- / header section -->