 <script type="text/javascript">
  $(function() {
    var Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      timer: 3000
    });  
    <?php  if(Session::has('success')) { ?>
      Toast.fire({
        icon: 'success',
        title: '<?php echo Session::get('success') ?>'
      });
    <?php } elseif (Session::has('info')) { ?>
        Toast.fire({
          icon: 'info',
          title: '<?php echo Session::get('info') ?>'
        });
    <?php } elseif (Session::has('error')) { ?>
        Toast.fire({
          icon: 'error',
          title: '<?php echo Session::get('error') ?>'
        });
    <?php } elseif (Session::has('warning')) { ?>
        Toast.fire({
          icon: 'warning',
          title: '<?php echo Session::get('warning') ?>'
        }); 
    <?php } ?>
  });
</script>


