  <!-- menu -->
  <section id="menu">
    <div class="container">
      <div class="row">
        <div class="menu-area">
          <!-- Navbar -->
          <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <button>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>  
              <span>All Categories</span>  
              <i class="fa fa-angle-down"></i>
              </a>     
            </div>
            <div class="navbar-collapse collapse">
              <!-- Left nav -->
              <ul class="nav navbar-nav">
                <li class="{{ areActiveRoutes(['home']) }}">
                  <a href="{{ route('home') }}">Home</a>
                </li>
                <li><a href="#">Burn <span class="caret"></span></a>
                  <ul class="dropdown-menu">                
                    <li><a href="#">Blends</a></li>
                    <li><a href="#">Resins</a></li>
                    <li><a href="#">Raw Oud</a></li>
                  </ul>
                </li>
                <li><a href="#">Oil <span class="caret"></span></a>
                  <ul class="dropdown-menu">  
                    <li><a href="#">Oud</a></li>                                                                
                    <li><a href="#">Florals</a></li>              
                    <li><a href="#">Blends</a></li>
                  </ul>
                </li>
                <li class="{{ areActiveRoutes(['about-us']) }}">
                  <a href="{{ route('about-us') }}">About</a>
                </li>
                <li class="{{ areActiveRoutes(['services']) }}">
                  <a href="{{ route('services') }}">Services</a>
                </li>
                <li class="{{ areActiveRoutes(['contact-us.index']) }}">
                  <a href="{{ route('contact-us.index') }}">Contact Us</a>
                </li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>     
      </div>  
    </div>
  </section>
  <!-- / menu -->