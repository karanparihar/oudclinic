<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{ URL::to('/') }}">
    <title>{{ config('app.name', 'Oudclinic') }} | @yield('title')</title>
    
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('assets/site/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/site/css/bootstrap.css') }}" rel="stylesheet">    
    <link href="{{ asset('assets/site/css/style.css') }}" rel="stylesheet">      
    <link href="{{ asset('assets/site/css/flickity.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/site/css/jquery.simpleLens.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>
<body>

    @include('layouts.inc._header')
    @include('layouts.inc._menu')
    @yield('content')
    @include('layouts.inc._footer')   

    <script src="{{ asset('assets/site/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/site/js/bootstrap.js') }}"></script> 
    <script src="{{ asset('assets/site/js/flickity.min.js') }}"></script>
    <script src="{{ asset('assets/site/js/jquery.simpleGallery.js') }}"></script>
    <script src="{{ asset('assets/site/js/jquery.simpleLens.js') }}"></script> 
    <script src="{{ asset('assets/site/js/custom.js') }}"></script>
    
    @if(Route::currentRouteName() == 'checkout.index') 
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
    $(function() {
    var $form         = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
    inputSelector = ['input[type=email]', 'input[type=password]',
    'input[type=text]', 'input[type=file]',
    'textarea'].join(', '),
    $inputs       = $form.find('.required').find(inputSelector),
    $errorMessage = $form.find('div.error'),
    valid         = true;
    $errorMessage.addClass('hide');
    $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
    var $input = $(el);
    if ($input.val() === '') {
    $input.parent().addClass('has-error');
    $errorMessage.removeClass('hide');
    e.preventDefault();
    }
    });
    if (!$form.data('cc-on-file')) {
    e.preventDefault();
    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
    Stripe.createToken({
    number: $('.card-number').val(),
    cvc: $('.card-cvc').val(),
    exp_month: $('.card-expiry-month').val(),
    exp_year: $('.card-expiry-year').val()
    }, stripeResponseHandler);
    }
    });
    function stripeResponseHandler(status, response) {
    if (response.error) {
    $('.error')
    .removeClass('hide')
    .find('.alert')
    .text(response.error.message);
    } else {
    /* token contains id, last4, and card type */
    var token = response['id'];
    $form.find('input[type=text]').empty();
    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
    $form.get(0).submit();
    }
    }
    });
    </script>
    @endif
</body>
</html>
