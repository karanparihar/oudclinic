<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'dev_restriction'  => 'This action is restricted for the production mode!',
    'role_restriction' => 'Unauthorised Action.',
    'password_not_match' => 'Your current password does not matches with the password you provided. Please try again.',
    'same_password' => 'New Password cannot be same as your current password. Please choose a different password.',
    
    'title'  => [
          'success'    => 'Success!',
          'warning'    => 'Warning!',
          'info'       => 'Info!',
          'error'      => 'Error!' 
    ],

    'created'         => ':model has been created successfully!',
    'archived'        => ':model has been archived successfully!',
    'failed'          => 'The action has been failed! Something went wrong!! See the log file for details.', 
    'updated'         => ':model has been updated successfully!',
    'deleted'         => ':model has been deleted successfully!',
    'status'          => ':model status has been changed successfully',
    'from_previous'   => 'A new sra has been created from the previous one',
    'password-update' => ':model has been updated successfully!',
 
   'ordering'  => [
        'form_fields' => ' Drag and Drop the Form fields to set in arranged order in this particular section.',
        'active_inactive' => 'All active and inactive fields show here to set the order but inactive fields will not be visible on SRA Form.',
    ],

    'alerts' => [
         'form_field' => 'No Form Field Found for this section.',
         'section'    => 'No section assigned to this SRA type.'
    ],
    'cart' => [
         'add'     => 'Product added to cart successfully!',
         'update'  => 'Cart has been updated successfully!'
    ],

    'order' => [
          'success' => 'Order has been placed successfully!',
          'error'   => 'Order Declined',
    ],
  
];