<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to display application language.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
     'model' => [
         'category'         => 'Category',
         'slider'           => 'Slider',
         'contact-request'  => 'Message',
         'testimonial'      => 'Testimonial',
         'product'          => 'Product',
         'order'            => 'Order'
         
     ],

     'buttons' => [
         'save'   => 'Save',
         'drart'  => 'Draft',
         'cancel' => 'Cancel'
     ],

     'actions' => [
        'edit'            => 'Edit',
        'trash'           => 'Delete',
        'view'            => 'View',
        'run'             => 'Run',
        'active'          =>  'Active',
        'inactive'        => 'In-Active',
        'proceed'         => 'Manage SRA',
        'pdf'             => 'View SRA',
        'change-password' => 'Change Password',
        'view_document'   => 'View Document',
        'generate_pdf'    => 'Generate Pdf',
        

     ],

     'action_class' => [
        'edit'   => 'warning',
        'view'   => 'success',
        'delete' => 'danger'
     ],

     'dropdown' => [
        'active'   => 'Active',
        'inactive' => 'In-Active',
        'table'    => 'Table',
        'form'     => 'Form',
        'option'   => 'Select an option'
     ],

     'currency' => '£',



];